//
//  GreenHouseRouter.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 01/12/2021.
//

import SwiftyJSON
import Alamofire

enum GreenHouseEndpoint {
//    case getListGreenHouse
    case getListSensor(greenHouseId: String)
    case getListRelayAndLed(greenHouseId: String, type: Int)
    case getListScenario(type: Int, greenHouseId: String)
    case getListRule(type: Int, greenHouseId: String)
    case getAGreenHouse(greenHouseId: String)
}

class GreenHouseRouter: BaseRouter {
    var endpoint: GreenHouseEndpoint

    init(endpoint: GreenHouseEndpoint) {
        self.endpoint = endpoint
    }

    override var baseURLString: String {
        return UserDefined.backendURLString
    }
    
     override var basePORTString: UInt16? {
         return UserDefined.backendPORT
    }

    override var contextPath: String {
        switch endpoint {
        case .getAGreenHouse, .getListSensor, .getListScenario, .getListRelayAndLed, .getListRule:
            return "/api/greenhouses"
        }
    }

    override var path: String {
        switch endpoint {
        case .getAGreenHouse(let greenHouseId):
            return "/\(greenHouseId)"
        case .getListSensor(let greenHouseId):
            return "/\(greenHouseId)/sensors"
        case .getListRelayAndLed(let greenHouseId, _):
            return "/\(greenHouseId)/relays"
        case .getListScenario(_, let greenHouseId), .getListRule(_, let greenHouseId):
            return "/\(greenHouseId)/rules"
        }
    }

    override var method: HTTPMethod {
        switch endpoint {
        case .getAGreenHouse, .getListSensor, .getListRelayAndLed, .getListScenario, .getListRule:
            return .get
        }
    }

    override var parameterEncoding: ParameterEncoding {
        switch endpoint {
        case .getAGreenHouse, .getListSensor, .getListRelayAndLed, .getListScenario, .getListRule:
            return URLEncoding.default
        }
    }

    override var parameters: [String: Any]? {
        var params = [String: Any]()

        switch endpoint {
        case .getAGreenHouse:
            params = [:]
        case .getListSensor:
            params = ["active": true]
        case .getListScenario(let type, _):
            params = ["type": type, "populate": UserDefined.populateID]
        case .getListRelayAndLed(_, let type):
            if type == -1 {
                params = ["active": true]
            } else {
                params = ["active": true, "type": type]
            }
        case .getListRule(let type, _):
            params = ["type": type, "populate": UserDefined.populateRuleID]
        }
        
        return params
    }
    
    override func successResponse(json: JSON) -> Any? {
        switch endpoint {
        case .getAGreenHouse:
            return GreenHouseModel(fromJSON: json["body"])
        case .getListSensor:
            return [SensorModel](fromJSON: json["body"]["results"])
        case .getListRelayAndLed:
            return [RelayModel](fromJSON: json["body"]["results"])
        case .getListScenario, .getListRule:
            return [RuleModel](fromJSON: json["body"]["results"])
        }
    }

    override func errorResponse(json: JSON) -> Any? {
        return ErrorModel(fromJSON: json)
    }
}

