//
//  AuthenticationRouter.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import Foundation
import SwiftyJSON
import Alamofire

enum AuthenticationEndpoint {
    case login(username: String, password: String)
    case refreshToken
    case logout
}

class AuthenticationRouter: BaseRouter {
    var endpoint: AuthenticationEndpoint

    init(endpoint: AuthenticationEndpoint) {
        self.endpoint = endpoint
    }

    override var baseURLString: String {
        return UserDefined.backendURLString
    }
    
     override var basePORTString: UInt16? {
         return UserDefined.backendPORT
    }

    override var contextPath: String {
        switch endpoint {
        case .login, .refreshToken, .logout:
            return "/api"
        }
    }

    override var path: String {
        switch endpoint {
        case .login:
            return "/auth/login"
        case .refreshToken:
            return "/auth/refresh-tokens"
        case .logout:
            return "/auth/logout"
        }
    }

    override var method: HTTPMethod {
        switch endpoint {
        case .login, .refreshToken, .logout:
            return .post
        }
    }

    override var parameterEncoding: ParameterEncoding {
        switch endpoint {
        case .login, .refreshToken, .logout:
            return JSONEncoding.default
        }
    }

    override var parameters: [String: Any]? {
        var params = [String: Any]()

        switch endpoint {
        case .login(let username, let password):
            params = ["username": username, "password": password]
        case .refreshToken, .logout:
            guard let currentUser = UserModel.shared.currentUser, let refreshToken = currentUser.token?.refreshToken?.token else { return nil}
            params = ["refreshToken": refreshToken]
        }
        
        return params
    }
    
    override func successResponse(json: JSON) -> Any? {
        switch endpoint {
        case .login:
            if let user = UserModel(fromJSON: json["body"]) {
                return user
            }
            return nil
        case .refreshToken:
            if let currentUser = UserModel.shared.currentUser, let token = json["body"]["token"].string {
                currentUser.token?.accessToken?.token = token
                UserModel.shared.save(currentUser)
                return (true, token)
            } else {
                return (false, "")
            }
        case .logout:
            return true
        }
    }

    override func errorResponse(json: JSON) -> Any? {
        return ErrorModel(fromJSON: json)
    }
}
