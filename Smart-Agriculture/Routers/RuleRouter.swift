//
//  ScheduleRouter.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 03/12/2021.
//

import Foundation
import Alamofire
import SwiftyJSON

enum RuleEndpoint {
    case getListScheduleOfDevice(type: Int, executeId: String)
    case updateSchedule(id: String, data: RuleModel)
    case addSchedule(data: RuleModel)
    case deleteRule(id: String)
    case updateScript(id: String, data: RuleModel)
    case addScript(data: RuleModel)
    case addRule(data: RuleModel)
    case updateRule(id: String, data: RuleModel)
}

class RuleRouter: BaseRouter {
    var endpoint: RuleEndpoint

    init(endpoint: RuleEndpoint) {
        self.endpoint = endpoint
    }

    override var baseURLString: String {
        return UserDefined.backendURLString
    }
    
     override var basePORTString: UInt16? {
         return UserDefined.backendPORT
    }

    override var contextPath: String {
        switch endpoint {
        case .getListScheduleOfDevice, .updateSchedule, .addSchedule, .deleteRule, .updateScript, .addScript, .addRule, .updateRule:
            return "/api"
        }
    }

    override var path: String {
        switch endpoint {
        case .getListScheduleOfDevice, .addSchedule, .addScript, .addRule:
            return "/rules"
        case .updateSchedule(let id, _), .deleteRule(let id), .updateScript(let id, _), .updateRule(let id, _):
            return "/rules/\(id)"
        }
    }

    override var method: HTTPMethod {
        switch endpoint {
        case .getListScheduleOfDevice:
            return .get
        case .updateSchedule, .updateScript, .updateRule:
            return .patch
        case .addSchedule, .addScript, .addRule:
            return .post
        case .deleteRule:
            return .delete
        }
    }

    override var parameterEncoding: ParameterEncoding {
        switch endpoint {
        case .getListScheduleOfDevice:
            return URLEncoding.default
        case .updateSchedule, .addSchedule, .deleteRule, .updateScript, .addScript, .addRule, .updateRule:
            return JSONEncoding.default
        }
    }

    override var parameters: [String: Any]? {
        var params = [String: Any]()

        switch endpoint {
        case .getListScheduleOfDevice(let type, let executeId):
            params = ["type": type, "executeId": executeId, "populate": UserDefined.populateID]
        case .updateSchedule(_ , let data):
            var newParams = [String: Any]()
            if let scheduleName = data.name {
                newParams["name"] = scheduleName
            }
            if let scheduleActive = data.active {
                newParams["active"] = scheduleActive
            }
            if let scheduleType = data.type {
                newParams["type"] = scheduleType
            }
            
            var scheduleParams = [String: Any]()
            if let timeStart = data.input.schedule.timeStart {
                scheduleParams = ["timeStart": timeStart]
            }
            var loopParams = [String: Any]()
            loopParams["monday"] = data.input.loop.monday
            loopParams["tuesday"] = data.input.loop.tuesday
            loopParams["wednesday"] = data.input.loop.wednesday
            loopParams["thursday"] = data.input.loop.thursday
            loopParams["friday"] = data.input.loop.friday
            loopParams["saturday"] = data.input.loop.saturday
            loopParams["sunday"] = data.input.loop.sunday
            var inputParams = [String: Any]()
            inputParams["schedule"] = scheduleParams
            inputParams["loop"] = loopParams
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                guard let usingExecute = execute.id as? RelayModel else { return nil }
                if let id = usingExecute.id {
                    executeInfo["ID"] = id
                }
                if let pull = execute.pull {
                    executeInfo["pull"] = pull
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                if let type = execute.type {
                    executeInfo["type"] = type
                }
                
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams["input"] = inputParams
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        case .addSchedule(let data):
            var newParams = [String: Any]()
            
            if let scheduleName = data.name {
                newParams["name"] = scheduleName
            }
            if let scheduleActive = data.active {
                newParams["active"] = scheduleActive
            }
            if let scheduleType = data.type {
                newParams["type"] = scheduleType
            }
            if let greenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId {
                newParams["greenhouseId"] = greenHouseId
            }
            
            var scheduleParams = [String: Any]()
            if let timeStart = data.input.schedule.timeStart {
                scheduleParams = ["timeStart": timeStart]
            }
            var loopParams = [String: Any]()
            loopParams["monday"] = data.input.loop.monday
            loopParams["tuesday"] = data.input.loop.tuesday
            loopParams["wednesday"] = data.input.loop.wednesday
            loopParams["thursday"] = data.input.loop.thursday
            loopParams["friday"] = data.input.loop.friday
            loopParams["saturday"] = data.input.loop.saturday
            loopParams["sunday"] = data.input.loop.sunday
            var inputParams = [String: Any]()
            inputParams["schedule"] = scheduleParams
            inputParams["loop"] = loopParams
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                guard let usingExecute = execute.id as? RelayModel else { return nil }
                if let id = usingExecute.id {
                    executeInfo["ID"] = id
                }
                if let pull = execute.pull {
                    executeInfo["pull"] = pull
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                if let type = execute.type {
                    executeInfo["type"] = type
                }
                
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams["input"] = inputParams
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        case .updateScript(_ , let data):
            var newParams = [String: Any]()
            if let scriptName = data.name {
                newParams["name"] = scriptName
            }
            if let scriptActive = data.active {
                newParams["active"] = scriptActive
            }
            if let scriptType = data.type {
                newParams["type"] = scriptType
            }
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                guard let usingExecute = execute.id as? RelayModel else { return nil }
                if let id = usingExecute.id {
                    executeInfo["ID"] = id
                }
                if let delay = execute.delay {
                    executeInfo["delay"] = delay
                }
                if let during = execute.during {
                    executeInfo["during"] = during
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                if let type = execute.type {
                    executeInfo["type"] = type
                }
                
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        case .addScript(let data):
            var newParams = [String: Any]()
            if let scriptName = data.name {
                newParams["name"] = scriptName
            }
            if let scriptActive = data.active {
                newParams["active"] = scriptActive
            }
            if let scriptType = data.type {
                newParams["type"] = scriptType
            }
            if let greenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId {
                newParams["greenhouseId"] = greenHouseId
            }
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                guard let usingExecute = execute.id as? RelayModel else { return nil }
                if let id = usingExecute.id {
                    executeInfo["ID"] = id
                }
                if let delay = execute.delay {
                    executeInfo["delay"] = delay
                }
                if let during = execute.during {
                    executeInfo["during"] = during
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                if let type = execute.type {
                    executeInfo["type"] = type
                }
                
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        case .updateRule(_ , let data):
            var newParams = [String: Any]()
            if let ruleName = data.name {
                newParams["name"] = ruleName
            }
            if let ruleActive = data.active {
                newParams["active"] = ruleActive
            }
            if let ruleType = data.type {
                newParams["type"] = ruleType
            }
            
            var scheduleParams = [String: Any]()
            if let timeStart = data.input.schedule.timeStart {
                scheduleParams["timeStart"] = timeStart
            }
            if let timeStop = data.input.schedule.timeStop {
                scheduleParams["timeStop"] = timeStop
            }
            if let timeUpdate = data.input.schedule.timeUpdate {
                scheduleParams["timeUpdate"] = timeUpdate
            }
            var loopParams = [String: Any]()
            loopParams["monday"] = data.input.loop.monday
            loopParams["tuesday"] = data.input.loop.tuesday
            loopParams["wednesday"] = data.input.loop.wednesday
            loopParams["thursday"] = data.input.loop.thursday
            loopParams["friday"] = data.input.loop.friday
            loopParams["saturday"] = data.input.loop.saturday
            loopParams["sunday"] = data.input.loop.sunday
            
            var logicParams = [String: Any]()
            if let conditionLogic = data.input.condition.logic {
                logicParams["logic"] = conditionLogic
            }
            var compareParams = ["compare": []]
            for compare in data.input.condition.compare {
                var compareInfo = [String: Any]()
                if let id = compare.id.id {
                    compareInfo["ID"] = id
                }
                if let equal = compare.equal {
                    compareInfo["equal"] = equal
                }
                if let value = compare.value {
                    compareInfo["value"] = value
                }
                compareParams["compare"]?.append(compareInfo)
            }
            logicParams.merge(compareParams) { (current, _) in current}
            
            var inputParams = [String: Any]()
            inputParams["schedule"] = scheduleParams
            inputParams["loop"] = loopParams
            inputParams["condition"] = logicParams
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                if let type = execute.type {
                    executeInfo["type"] = type
                    
                    if type == ExecuteType.relay.rawValue {
                        guard let usingExecute = execute.id as? RelayModel else { return nil }
                        if let id = usingExecute.id {
                            executeInfo["ID"] = id
                        }
                    } else {
                        guard let usingExecute = execute.id as? RuleModel else { return nil }
                        if let id = usingExecute.id {
                            executeInfo["ID"] = id
                        }
                    }
                }
                if let delay = execute.delay {
                    executeInfo["delay"] = delay
                }
                if let during = execute.during {
                    executeInfo["during"] = during
                }
                if let pull = execute.pull {
                    executeInfo["pull"] = pull
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams["input"] = inputParams
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        case .addRule(let data):
            var newParams = [String: Any]()
            if let ruleName = data.name {
                newParams["name"] = ruleName
            }
            if let ruleActive = data.active {
                newParams["active"] = ruleActive
            }
            if let ruleType = data.type {
                newParams["type"] = ruleType
            }
            if let greenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId {
                newParams["greenhouseId"] = greenHouseId
            }
            
            var scheduleParams = [String: Any]()
            if let timeStart = data.input.schedule.timeStart {
                scheduleParams["timeStart"] = timeStart
            }
            if let timeStop = data.input.schedule.timeStop {
                scheduleParams["timeStop"] = timeStop
            }
            if let timeUpdate = data.input.schedule.timeUpdate {
                scheduleParams["timeUpdate"] = timeUpdate
            }
            var loopParams = [String: Any]()
            loopParams["monday"] = data.input.loop.monday
            loopParams["tuesday"] = data.input.loop.tuesday
            loopParams["wednesday"] = data.input.loop.wednesday
            loopParams["thursday"] = data.input.loop.thursday
            loopParams["friday"] = data.input.loop.friday
            loopParams["saturday"] = data.input.loop.saturday
            loopParams["sunday"] = data.input.loop.sunday
            
            var logicParams = [String: Any]()
            if let conditionLogic = data.input.condition.logic {
                logicParams["logic"] = conditionLogic
            }
            var compareParams = ["compare": []]
            for compare in data.input.condition.compare {
                var compareInfo = [String: Any]()
                if let id = compare.id.id {
                    compareInfo["ID"] = id
                }
                if let equal = compare.equal {
                    compareInfo["equal"] = equal
                }
                if let value = compare.value {
                    compareInfo["value"] = value
                }
                compareParams["compare"]?.append(compareInfo)
            }
            logicParams.merge(compareParams) { (current, _) in current}
            
            var inputParams = [String: Any]()
            inputParams["schedule"] = scheduleParams
            inputParams["loop"] = loopParams
            inputParams["condition"] = logicParams
            
            var executeParams = ["execute": []]
            for execute in data.execute {
                var executeInfo = [String: Any]()
                if let type = execute.type {
                    executeInfo["type"] = type
                    
                    if type == ExecuteType.relay.rawValue {
                        guard let usingExecute = execute.id as? RelayModel else { return nil }
                        if let id = usingExecute.id {
                            executeInfo["ID"] = id
                        }
                    } else {
                        guard let usingExecute = execute.id as? RuleModel else { return nil }
                        if let id = usingExecute.id {
                            executeInfo["ID"] = id
                        }
                    }
                }
                if let delay = execute.delay {
                    executeInfo["delay"] = delay
                }
                if let during = execute.during {
                    executeInfo["during"] = during
                }
                if let pull = execute.pull {
                    executeInfo["pull"] = pull
                }
                if let dim = execute.dim {
                    executeInfo["DIM"] = dim
                }
                if let cct = execute.cct {
                    executeInfo["CCT"] = cct
                }
                executeParams["execute"]?.append(executeInfo)
            }
            
            newParams["input"] = inputParams
            newParams.merge(executeParams) { (current, _) in current }
            
            params = newParams
        default:
            params = [:]
        }
        
        return params
    }
    
    override func successResponse(json: JSON) -> Any? {
        switch endpoint {
        case .getListScheduleOfDevice:
            return [RuleModel](fromJSON: json["body"]["results"])
        case .updateSchedule, .addSchedule, .deleteRule, .updateScript, .addScript, .addRule, .updateRule:
            return true
        }
    }

    override func errorResponse(json: JSON) -> Any? {
        return ErrorModel(fromJSON: json)
    }
}
