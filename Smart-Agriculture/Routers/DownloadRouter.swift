//
//  DownloadRouter.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 14/01/2022.
//

import Foundation
import Alamofire
import SwiftyJSON

enum DownloadEndpoint {
    case downloadOne(id: String, nDays: Int, date: String)
    case downloadAll(id: String, nDays: Int, date: String)
}

class DownloadRouter: BaseRouter {
    var endpoint: DownloadEndpoint

    init(endpoint: DownloadEndpoint) {
        self.endpoint = endpoint
    }

    override var baseURLString: String {
        return UserDefined.backendURLString
    }
    
     override var basePORTString: UInt16? {
         return UserDefined.backendPORT
    }

    override var contextPath: String {
        switch endpoint {
        case .downloadOne, .downloadAll:
            return "/api"
        }
    }

    override var path: String {
        switch endpoint {
        case .downloadAll(let id, _, _):
            return "/greenhouses/\(id)/sensor_values/export"
        case .downloadOne(let id, _, _):
            return "/sensors/\(id)/sensor_values/export"
        }
    }

    override var method: HTTPMethod {
        switch endpoint {
        case .downloadAll, .downloadOne:
            return .get
        }
    }

    override var parameterEncoding: ParameterEncoding {
        switch endpoint {
        case .downloadAll, .downloadOne:
            return URLEncoding.default
        }
    }

    override var parameters: [String: Any]? {
        var params = [String: Any]()

        switch endpoint {
        case .downloadAll(_, let nDays, let date), .downloadOne(_, let nDays, let date):
            params["nDays"] = nDays
            params["date"] = date
        }
        
        return params
    }
    
    override func successResponse(json: JSON) -> Any? {
        switch endpoint {
        case .downloadAll, .downloadOne:
            return true
        }
    }

    override func errorResponse(json: JSON) -> Any? {
        return ErrorModel(fromJSON: json)
    }
}
