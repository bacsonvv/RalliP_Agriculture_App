//
//  RelayRouter.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 13/01/2022.
//

import Foundation
import Alamofire
import SwiftyJSON

enum RelayEndpoint {
    case updateRelay(relay: RelayModel)
}

class RelayRouter: BaseRouter {
    var endpoint: RelayEndpoint

    init(endpoint: RelayEndpoint) {
        self.endpoint = endpoint
    }

    override var baseURLString: String {
        return UserDefined.backendURLString
    }
    
     override var basePORTString: UInt16? {
         return UserDefined.backendPORT
    }

    override var contextPath: String {
        switch endpoint {
        case .updateRelay:
            return "/api"
        }
    }

    override var path: String {
        switch endpoint {
        case .updateRelay(let relay):
            return "/relays/\(relay.id ?? "")"
        }
    }

    override var method: HTTPMethod {
        switch endpoint {
        case .updateRelay:
            return .patch
        }
    }

    override var parameterEncoding: ParameterEncoding {
        switch endpoint {
        case .updateRelay:
            return JSONEncoding.default
        }
    }

    override var parameters: [String: Any]? {
        var params = [String: Any]()

        switch endpoint {
        case .updateRelay(let relay):
            var newParams = [String: Any]()
            if let relayName = relay.name {
                newParams["name"] = relayName
            }
            if let relayMac = relay.mac {
                newParams["mac"] = relayMac
            }
            if let timeUpdate = relay.timeUpdate {
                newParams["timeUpdate"] = timeUpdate
            }
            if let relayStatus = relay.status {
                newParams["status"] = relayStatus
            }
            
            var valueParams = ["values": []]
            for value in relay.value {
                var valueInfo = [String: Any]()
                if let valueName = value.name {
                    valueInfo["name"] = valueName
                }
                if let valueData = value.value {
                    valueInfo["value"] = valueData
                }
                valueParams["values"]?.append(valueInfo)
            }
            newParams.merge(valueParams) { (current, _) in current}
            
            params = newParams
        }
        
        return params
    }
    
    override func successResponse(json: JSON) -> Any? {
        switch endpoint {
        case .updateRelay:
            return true
        }
    }

    override func errorResponse(json: JSON) -> Any? {
        return ErrorModel(fromJSON: json)
    }
}
