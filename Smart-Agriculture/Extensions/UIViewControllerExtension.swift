//
//  UIViewControllerExtension.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import UIKit
import KVLoading

extension UIViewController {
    @IBAction func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func goBackToPreviousViewController() {
        if let navigationController = navigationController, navigationController.viewControllers.count > 1 {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func goBackToRootViewController() {
        if let navigationController = navigationController, navigationController.viewControllers.count > 1 {
            navigationController.popToRootViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: KVLoading
extension UIViewController {
    @objc func showLoadingInView(_ view: UIView, timeout: Int = 60, _ customView: UIView? = nil, animated: Bool = true) {
        DispatchQueue.main.async {
            UIView.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.hideLoading(animated:)), object: nil)
            self.perform(#selector(self.hideLoading(animated:)), with: nil, afterDelay: TimeInterval(timeout))
            KVLoading.shared.showInView(view: view, customView: customView, animated: animated)
        }
    }

    @objc func showLoading(timeout: Int = 60, _ customView: UIView? = nil, animated: Bool = true) {
        var originalKeyWindow: UIWindow?
        #if swift(>=5.1)
        if #available(iOS 13, *) {
            originalKeyWindow = UIApplication.shared.connectedScenes
                .compactMap { $0 as? UIWindowScene }
                .flatMap { $0.windows }
                .first(where: { $0.isKeyWindow })
        } else {
            originalKeyWindow = UIApplication.shared.keyWindow
        }
        #else
        originalKeyWindow = UIApplication.shared.keyWindow
        #endif
        guard let window = originalKeyWindow else { return }
        DispatchQueue.main.async {
            self.showLoadingInView(window, timeout: timeout, customView, animated: animated)
        }
    }

    @objc func hideLoading(animated: Bool = true) {
        DispatchQueue.main.async {
            KVLoading.shared.hide(animated: animated)
        }
    }
}
