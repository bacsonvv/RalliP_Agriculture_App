//
//  UIImageExtension.swift
//  Smart-Agriculture
//
//  Created by admin on 05/11/2021.
//

import UIKit

extension UIImageView {
    public func maskCircle() {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
}
