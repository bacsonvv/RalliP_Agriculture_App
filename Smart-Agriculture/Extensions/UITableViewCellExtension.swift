//
//  UITableViewCellExtension.swift
//  Smart-Agriculture
//
//  Created by admin on 04/11/2021.
//

import UIKit

extension UITableViewCell {
    func roundedCellCorner(view: UIView) {
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 6
    }
}
