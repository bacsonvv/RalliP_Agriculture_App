//
//  UIButtonExtension.swift
//  Smart-Agriculture
//
//  Created by admin on 04/11/2021.
//

import UIKit

extension UISwitch {
    func scale(x: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: x, y: y)
    }
}

extension UIButton {
    func customize(backgroundColor: UIColor, tintColor: UIColor, cornerRadius: CGFloat) {
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
        self.layer.cornerRadius = cornerRadius
    }
}
