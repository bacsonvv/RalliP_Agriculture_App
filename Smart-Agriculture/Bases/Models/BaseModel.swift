//
//  BaseModel.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import Foundation
import SwiftyJSON

class BaseModel: NSObject, SwiftyJSONMappable {
    var objectId: Any?

    convenience required init?(fromJSON json: JSON) {
        self.init()
        mapping(fromJSON: json)
    }

    convenience required init?(fromString string: String) {
        self.init()
        let json = string.toJSON()
        mapping(fromJSON: json)
    }

    convenience required init?(fromDictionary dictionary: Dictionary<String, Any>) {
        self.init()
        fromDictionary(dictionary)
    }

    func mapping(fromJSON json: JSON) {
        objectId = json["id"].string
    }

    func toDictionary() -> Dictionary<String, Any> {
        return Dictionary<String, Any>()
    }

    func fromDictionary(_ dictionary: Dictionary<String, Any>) {}
}

protocol SwiftyJSONMappable {
    init?(fromJSON json: JSON)
    init?(fromString string: String)
}

extension Array where Element: SwiftyJSONMappable {
    init(fromJSON json: JSON) {
        self.init()
        if json.type == .null { return }
        for item in json.arrayValue {
            if let object = Element.init(fromJSON: item) {
                self.append(object)
            }
        }
    }

    init(fromString string: String) {
        self.init()
        let json = string.toJSON()
        if json.type == .null { return }
        for item in json.arrayValue {
            if let object = Element.init(fromJSON: item) {
                self.append(object)
            }
        }
    }
}

extension String {
    func toJSON() -> JSON {
        return JSON(parseJSON: self)
    }
}

extension Dictionary {
    func toJSON() -> String {
        var error : NSError?

        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)

        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String

        return jsonString
    }
}
