//
//  ErrorModel.swift
//  Smart-Agriculture
//
//  Created by admin on 08/11/2021.
//

import SwiftyJSON

class ErrorModel: BaseModel {
    var code: Int?
    var message: String?

    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        code = json["code"].int
        message = json["message"].string
    }
}
