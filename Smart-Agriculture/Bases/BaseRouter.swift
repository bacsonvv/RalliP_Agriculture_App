//
//  BaseRouter.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import Alamofire
import Foundation
import Kingfisher
import SwiftyJSON

enum APIResult {
    case success(Any?)
    case failure(Any?)
    case forcedUpdate(Any?)
    case networkError
    case maintained(Any?)
    case timedOut
    case unauthorized(Any?)
    case undefined
}

class APICache {
    static var shared = APICache()

    var isGettingNewToken = false
    var isTrackingLoadCoupons = false
    var numberOfAPIRequest = 0
}

class BaseRouter {
    private let sessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.headers = .default
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        configuration.timeoutIntervalForRequest = 59
        configuration.timeoutIntervalForResource = 299
        return Session(configuration: configuration)
    }()

    private var fullURL: URLConvertible {
        guard let port = basePORTString else {
            return (URL(string: "\(baseURLString)\(contextPath)\(path)"))!
        }

        return (URL(string: "\(baseURLString):\(port)\(contextPath)\(path)"))!
    }

    var baseURLString: String {
        return ""
    }

    var basePORTString: UInt16? {
        return nil
    }

    var contextPath: String {
        return ""
    }

    var path: String {
        fatalError("[\(#function))] Must be overridden in subclass")
    }

    var method: HTTPMethod {
        fatalError("[\(#function))] Must be overridden in subclass")
    }

    var headerFields: HTTPHeaders? {
        if let currentUser = UserModel.shared.currentUser, let token = currentUser.token?.accessToken?.token {
            return HTTPHeaders([
                "Authorization": "Bearer \(token)",
                "Content-Type": "application/json",
            ])
        } else {
            return HTTPHeaders([
                "Content-Type": "application/json"
            ])
        }

    }

    var parameterEncoding: ParameterEncoding {
        fatalError("[\(#function))] Must be overridden in subclass")
    }

    var parameters: [String: Any]? {
        return nil
    }

    func successResponse(json: JSON) -> Any? {
        return nil
    }

    func successResponse(data: Data) -> Any? {
        return nil
    }

    func errorResponse(json: JSON) -> Any? {
        return nil
    }

    var skip: Int = 0
    var limit: Int = 10

    func request(completed: ((APIResult) -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            var clientResponse: APIResult = .undefined {
                didSet {
                    DispatchQueue.main.async {
                        completed?(clientResponse)
                    }
                }
            }

//            #if DEVELOPMENT
            print("- REQUEST:\n - URL:\n\(self.fullURL)\n - parameters:\n\(self.parameters?.toJSON())\n - headers:\n\(self.headerFields?.dictionary.toJSON())")
//            #endif
            guard let networkReachabilityManager = NetworkReachabilityManager(), networkReachabilityManager.isReachable else {
//                #if DEVELOPMENT
                print("- RESPONSE:\n- Not connected to internet")
//                #endif
                clientResponse = .networkError
                return
            }

            self.sessionManager.request(self.fullURL, method: self.method, parameters: self.parameters, encoding: self.parameterEncoding, headers: self.headerFields).response { (serverResponse) in
                if let resultResponse = serverResponse.response {
//                    #if DEVELOPMENT
                    print("- StatusCode: \(resultResponse.statusCode)")
//                    #endif
                    if let url = resultResponse.url {
//                        #if DEVELOPMENT
                        print("- Request URL: \(url.absoluteString)")
//                        #endif
                    }
                    if let data = serverResponse.data {
                        do {
                            let json = try JSON(data: data)
//                            #if DEVELOPMENT
                            print("- RESPONSE:\n\(json)")
//                            #endif
                            switch resultResponse.statusCode {
                            case 200 ... 299:
                                if let successEntity = self.successResponse(json: json) {
                                    if let status = json["code"].int, status == 200 || status == 201 || status == 204 {
                                        clientResponse = .success(successEntity)
                                    } else if let errorEntity = self.errorResponse(json: json) {
                                        if let errorCode = json["code"].int, errorCode == 401 {
                                            clientResponse = .unauthorized(errorEntity)
                                        }
                                    } else {
                                        clientResponse = .undefined
                                    }
                                } else {
                                    if let errorEntity = self.errorResponse(json: json) {
                                        if let errorCode = json["code"].string, errorCode == "CM0004" {
                                            clientResponse = .unauthorized(errorEntity)
                                        } else if let errorCode = json["code"].string, errorCode == "CM0009" {
                                            clientResponse = .unauthorized(errorEntity)
                                        } else if let errorCode = json["code"].string, errorCode == "CM00010" {
                                            clientResponse = .maintained(errorEntity)
                                        } else if let errorCode = json["code"].string, errorCode == "CM00011" {
                                            clientResponse = .forcedUpdate(errorEntity)
                                        } else {
                                            clientResponse = .failure(errorEntity)
                                        }
                                    } else {
                                        clientResponse = .undefined
                                    }
                                }

                            case 401:
                                if let errorEntity = self.errorResponse(json: json) {
                                    clientResponse = .unauthorized(errorEntity)
                                } else {
                                    clientResponse = .unauthorized(nil)
                                }

                            default:
                                if let errorEntity = self.errorResponse(json: json) {
                                    clientResponse = .failure(errorEntity)
                                } else {
                                    clientResponse = .undefined
                                }
                            }
                        } catch {
                            switch resultResponse.statusCode {
                            case 200 ... 299:
                                if let successEntity = self.successResponse(data: data) {
//                                    #if DEVELOPMENT
                                    print("- RESPONSE:\n\(successEntity)")
//                                    #endif
                                    clientResponse = .success(successEntity)
                                } else {
//                                    #if DEVELOPMENT
                                    print("- RESPONSE:\n- nil")
//                                    #endif
                                    clientResponse = .undefined
                                }

                            case 401:
                                clientResponse = .unauthorized(nil)

                            default:
//                                #if DEVELOPMENT
                                print("- RESPONSE:\n- nil\n")
//                                #endif
                                clientResponse = .undefined
                            }
                        }
                    } else {
//                        #if DEVELOPMENT
                        print("- RESPONSE:\n- nil")
//                        #endif
                        clientResponse = .undefined
                    }
                } else {
                    if let error = serverResponse.error, let underlyingError = error.underlyingError as NSError? {
//                        #if DEVELOPMENT
                        print("ErrorCode: \(underlyingError.code)")
                        print("ErrorDescription: \(underlyingError.localizedDescription)")
//                        #endif
                        switch underlyingError.code {
                        case NSURLErrorNotConnectedToInternet:
//                            #if DEVELOPMENT
                            print("- RESPONSE:\n- NSURLErrorNotConnectedToInternet")
//                            #endif
                            clientResponse = .networkError
                        case NSURLErrorTimedOut:
//                            #if DEVELOPMENT
                            print("- RESPONSE:\n- NSURLErrorTimedOut")
//                            #endif
                            clientResponse = .timedOut
                        default:
//                            #if DEVELOPMENT
                            print("- RESPONSE:\n- nil")
//                            #endif
                            clientResponse = .undefined
                        }
                    } else {
//                        #if DEVELOPMENT
                        print("- RESPONSE:\n- nil")
//                        #endif
                        clientResponse = .undefined
                    }
                }
            }
        }
    }

    func mapTo<T>(_ model: T.Type, continue: Bool = false, indicator: Bool = true, minimumInterval: CGFloat = 0, success: @escaping ((T) -> Void), failure: ((Any?) -> Void)? = nil, forcedUpdate: ((Any?) -> Void)? = nil, networkError: (() -> Void)? = nil, maintained: ((Any?) -> Void)? = nil, timedOut: (() -> Void)? = nil, unauthorized: ((Any?) -> Void)? = nil, undefined: (() -> Void)? = nil, didComplete: (() -> Void)? = nil) {
        var isHandleComplete = true
        var isShownIndicator = false
        if let topViewController = UIApplication.topViewController(), indicator {
            isShownIndicator = true
            topViewController.showLoading()
        }

        let startDate = Date()
        request { (result) in
            var isUndefined = false
            if !`continue`, let topViewController = UIApplication.topViewController(), isShownIndicator {
                topViewController.hideLoading()
            }

            if minimumInterval > 0 {
                let interval = Date().timeIntervalSince(startDate)
                if minimumInterval > CGFloat(interval) {
                    let sleepValue = useconds_t((minimumInterval - CGFloat(interval)) * 1000000)
                    if sleepValue > 0 {
                        usleep(sleepValue)
                    }
                }
            }

            switch result {
            case .success(let response):
                if let value = response as? T {
                    success(value)
                } else {
                    isUndefined = true
                }

            case .failure(let error):
                if let closure = failure {
                    closure(error)
                } else {
                    if let error = error as? ErrorModel {
//                        if let errorCode = error.code, let message = error.message {
//                            guard let topViewController = UIApplication.topViewController() else { return }
//                            DispatchQueue.main.async {
//                                topViewController.hideLoading()
//                                switch errorCode {
//                                case "CM0000":
//                                    topViewController.showAlert(title: "Something went wrong title".localized(), subtitle: "Something went wrong".localized(), cancelTitle: "OK".localized())
//                                case "API105_ER01",
//                                     "API100_ER03",
//                                     "API100_ER04",
//                                     "API109_ER02":
//                                    topViewController.showAlert(title: "Verification fail popup title".localized(), subtitle: message, cancelTitle: "OK".localized())
//                                case "CM0004", "CM0009":
//                                    topViewController.showAlert(title: "Notification".localized(), subtitle: message, cancelTitle: "OK".localized(), cancelAction: {
//                                        DispatchQueue.main.async {
//                                            APICache.shared.numberOfAPIRequest = 0
//                                            UserModel.shared.deleteAllData()
//                                            if let keyWindow = UIApplication.getKeyWindow() {
//                                                let login = R.storyboard.login.memberLoginNavigationController()
//                                                keyWindow.rootViewController = login
//
//                                                login?.hideLoading()
//                                            }
//                                        }
//                                    })
//                                default:
//                                    topViewController.showAlert(title: "Notification".localized(), subtitle: message, cancelTitle: "OK".localized())
//                                }
//                                print("FAILURE")
//                            }
//                        } else {
//                            isUndefined = true
//                        }
                        print("FAILURE")
                    } else {
                        isUndefined = true
                    }
                }

            case .forcedUpdate(let response):
                if let closure = forcedUpdate {
                    closure(response)
                } else {
//                    if let keyWindow = UIApplication.getKeyWindow(),
//                       let maintainedScreen = R.storyboard.maintenace.maintenaceViewController() {
//                        maintainedScreen.screenType = .forcedUpdate
//                        if let error = response as? ErrorModel, let message = error.message {
//                            maintainedScreen.messageFromServer = message
//                        }
//                        keyWindow.rootViewController = maintainedScreen
//                    }
                    print("FORCED UPDATE")
                }

            case .networkError:
                if let closure = networkError {
                    closure()
                } else {
                    guard let topViewController = UIApplication.topViewController() else { return }
                    DispatchQueue.main.async {
                        topViewController.hideLoading()
//                        topViewController.showAlert(title: "Network error title".localized(), subtitle: "No internet connection".localized(), cancelTitle: "OK".localized())
                        print("NETWORK ERROR")
                    }
                }

            case .maintained(let response):
                if let closure = maintained {
                    closure(response)
                } else {
//                    if let keyWindow = UIApplication.getKeyWindow(),
//                       let maintainedScreen = R.storyboard.maintenace.maintenaceViewController() {
//                        maintainedScreen.screenType = .maintained
//                        if let error = response as? ErrorModel, let message = error.message {
//                            maintainedScreen.messageFromServer = message
//                        }
//                        keyWindow.rootViewController = maintainedScreen
//                    }
                    print("MAINTAINED")
                }

            case .timedOut:
                if let closure = timedOut {
                    closure()
                } else {
                    isUndefined = true
                }

            case .unauthorized(let response):
                if let closure = unauthorized {
                    closure(response)
                } else {
                    if let error = response as? ErrorModel, let errorCode = error.code {
                        func getNewToken() {
                            DispatchQueue.main.async {
                                APICache.shared.numberOfAPIRequest += 1
                                AuthenticationRouter(endpoint: .refreshToken)
                                    .mapTo((Bool, String?).self, indicator: indicator, success: { (isSuccess, token) in
                                        DispatchQueue.main.async {
                                            if isSuccess {
                                                if let currentUser = UserModel.shared.currentUser, let value = token {
                                                    currentUser.token?.accessToken?.token = value
                                                    UserModel.shared.save(currentUser)
                                                }
                                                
                                                APICache.shared.isGettingNewToken = false
                                                APICache.shared.numberOfAPIRequest = 0
                                                self.mapTo(model, continue: `continue`, indicator: indicator, minimumInterval: minimumInterval, success: success, failure: failure, forcedUpdate: forcedUpdate, networkError: networkError, maintained: maintained, timedOut: timedOut, unauthorized: unauthorized, undefined: undefined, didComplete: didComplete)
                                            } else {
                                                if APICache.shared.numberOfAPIRequest < 3 {
                                                    getNewToken()
                                                } else {
                                                    logout()
                                                }
                                            }
                                        }
                                    }, failure: { _ in
                                        logout()
                                    }, forcedUpdate: { _ in
                                        logout()
                                    }, networkError: {
                                        logout()
                                    }, maintained: { _ in
                                        logout()
                                    }, timedOut: {
                                        logout()
                                    }, unauthorized: { _ in
                                        logout()
                                    }, undefined: {
                                        logout()
                                    })
                            }
                        }
                        
                        func logout() {
                            DispatchQueue.main.async {
                                APICache.shared.isGettingNewToken = false
                                APICache.shared.numberOfAPIRequest = 0
                                UserModel.shared.deleteAllData()
                                if let keyWindow = UIApplication.getKeyWindow() {
                                    let login = R.storyboard.login.loginNavID()
                                    keyWindow.rootViewController = login
                                    
                                    login?.hideLoading()
                                }
                            }
                        }
                        
                        func run() {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                //                                guard UserModel.shared.isLoggedIn else { return }
                                if APICache.shared.isGettingNewToken {
                                    run()
                                } else {
                                    self.mapTo(model, continue: `continue`, indicator: indicator, minimumInterval: minimumInterval, success: success, failure: failure, forcedUpdate: forcedUpdate, networkError: networkError, maintained: maintained, timedOut: timedOut, unauthorized: unauthorized, undefined: undefined, didComplete: didComplete)
                                }
                            }
                        }
                        
                        isHandleComplete = false
                        if APICache.shared.isGettingNewToken {
                            run()
                        } else {
                            APICache.shared.isGettingNewToken = true
                            getNewToken()
                        }
                    }
                    //                        switch errorCode {
                    //                        case "CM0009":
                    //                            DispatchQueue.main.async {
                    //                                if UserModel.shared.isLoggedIn {
                    //                                    UserModel.shared.deleteAllData()
                    //                                    if let keyWindow = UIApplication.getKeyWindow() {
                    //                                        let login = R.storyboard.login.memberLoginNavigationController()
                    //                                        keyWindow.rootViewController = login
                    //                                    }
                    //
                    //                                    guard let topViewController = UIApplication.topViewController() else { return }
                    //                                    topViewController.hideLoading()
                    //
                    //                                    // Show logged out alert
                    //                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    //                                        if let message = error.message, !message.isEmpty {
                    //                                            topViewController.showAlert(title: "Notification".localized(), subtitle: message, cancelTitle: "Okay".localized(), cancelAction: nil, force: true)
                    //                                        }
                    //                                    }
                    //                                }
                    //                            }
                    //                        default:
                    //
                }

            default:
                isUndefined = true
            }

//            if isUndefined {
//                if let closure = undefined {
//                    closure()
//                } else {
//                    guard let topViewController = UIApplication.topViewController() else { return }
//                    DispatchQueue.main.async {
//                        topViewController.hideLoading()
//                        topViewController.showAlert(title: "Something went wrong title".localized(), subtitle: "Something went wrong".localized(), cancelTitle: "OK".localized())
//                    }
//                }
//            }

            if let closure = didComplete, isHandleComplete {
                closure()
            }
        }
    }
}
