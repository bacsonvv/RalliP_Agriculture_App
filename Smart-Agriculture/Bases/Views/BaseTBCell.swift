//
//  BaseTBCell.swift
//  KoshikiApp
//
//  Created by Vuong Vu Bac Son on 08/03/2021.
//

import UIKit

class BaseTBCell: UITableViewCell {
    weak var parentViewController: UIViewController?
    
    static func identifier() -> String {
        return String(describing: self.self)
    }

    static func height() -> CGFloat {
        return 0
    }

    static func registerCellByClass(_ tableView: UITableView) {
        tableView.register(self.self, forCellReuseIdentifier: self.identifier())
    }

    static func registerCellByNib(_ tableView: UITableView) {
        let nib = UINib(nibName: self.identifier(), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: self.identifier())
    }

    static func loadCell(_ tableView: UITableView) -> BaseTBCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier()) as? BaseTBCell {
            return cell
        } else {
            fatalError()
        }
    }
}
