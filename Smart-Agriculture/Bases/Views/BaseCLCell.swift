//
//  BaseCLCell.swift
//  KoshikiApp
//
//  Created by Vuong Vu Bac Son on 08/03/2021.
//

import UIKit

class BaseCLCell: UICollectionViewCell {
    static func identifier() -> String {
        return String(describing: self.self)
    }

    static func size() -> CGSize {
        return CGSize.zero
    }

    static func registerCellByClass(_ collectionView: UICollectionView) {
        collectionView.register(self.self, forCellWithReuseIdentifier: self.identifier())
    }

    static func registerCellByNib(_ collectionView: UICollectionView) {
        let nib = UINib(nibName: self.identifier(), bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: self.identifier())
    }

    static func loadCell(_ collectionView: UICollectionView, path: IndexPath) -> BaseCLCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.identifier(), for: path) as? BaseCLCell {
            return cell
        } else {
            fatalError()
        }
    }
}
