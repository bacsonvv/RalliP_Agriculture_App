//
//  BaseViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import UIKit

class BaseViewController: UIViewController {
    @IBOutlet weak var lblBaseScreenTitle: UILabel!
    weak var parentController: UIViewController?
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
        
        // Add gesture for view - hide keyboard when tap on the screen
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            view.endEditing(true)
        }
        sender.cancelsTouchesInView = false
    }
}
