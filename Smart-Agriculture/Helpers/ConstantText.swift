//
//  ConstantText.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import Foundation

enum GreenHouseMenuTitle: String {
    case sensor = "CẢM BIẾN"
    case relay = "RƠ-LE"
    case led = "LED"
}

enum MenuItem: String {
    case greenHouseSection = "NHÀ KÍNH"
    case greenHouseItem = "Nhà kính"
    case settings = "CÀI ĐẶT"
    case setSchedule = "Lập lịch"
    case scenario = "Kịch bản"
    case rules = "Luật điều khiển"
    case myAccount = "TÀI KHOẢN CỦA TÔI"
    case notification = "Thông báo"
    case logout = "Đăng xuất"
    
    func getTitle() -> String {
        return self.rawValue
    }
}

enum DevicePropertyName: String {
    // Light property names
    case dim = "DIM"
    case cct = "CCT"
}

enum SwitchState: String {
    case on = "Đang bật"
    case off = "Đã tắt"
}

enum OnOffState: Int {
    case off = 0
    case on = 1
}

enum DeviceType: Int {
    case relay = 0
    case led = 1
}

enum RuleType: Int {
    case script = 2
    case rule = 1
    case schedule = 0
}

enum RepeatText: String {
    case t2 = "T2"
    case t3 = "T3"
    case t4 = "T4"
    case t5 = "T5"
    case t6 = "T6"
    case t7 = "T7"
    case cn = "CN"
    case everyDay = "Hàng ngày"
}

enum DeviceTypeName: String {
    case pump = "Máy bơm"
    case light = "Độ sáng"
}

enum ScreenType: String {
    case add = "Thêm"
    case edit = "Sửa"
}

enum ExecuteType: String {
    case relay = "Relay"
    case script = "Script"
}

enum TimeType: String {
    case start = "START"
    case stop = "STOP"
}

enum LogicType: String {
    case and = "AND"
    case or = "OR"
}

enum LogicTypeText: String {
    case and = "VÀ"
    case or = "HOẶC"
}

enum SensorType: Int {
    case temperature = 0
    case humidity = 1
    case soil = 2
    case light = 3
    case co2 = 4
}

enum SensorTypeName: String {
    case temperature = "Nhiệt độ"
    case humidity = "Độ ẩm không khí"
    case soil = "Độ ẩm đất"
    case light = "Ánh sáng"
    case co2 = "Nồng độ CO2"
}

enum SensorUnitType: String {
    case temperature = "độ C"
    case light = "lux"
    case soilAndHumidity = "%"
    case co2 = "ppm"
}

enum CompareEqualType: String {
    case greater = ">"
    case lessThan = "<"
}

class ConstantText {
    static var alertTitle = "Thông báo"
    static var forgotPasswordMessage = "Vui lòng liên hệ quản trị viên"
    static var undefinedText = "Không xác định"
    static var okAlertButtonTitle = "OK"
    static var confirmAlertButtonTitle = "Có"
    static var cancelAlertButtonTitle = "Không"
    static var confirmDeleteMessage = "Bạn có chắc chắn muốn xóa dữ liệu hay không? (Sau khi xóa dữ liệu sẽ không thể khôi phục)"
    static var deleteSuccessMessage = "Dữ liệu đã được xóa thành công"
    static var deleteFailMessage = "Dữ liệu xóa không thành công"
    static var refreshDataMessage = "Làm mới dữ liệu..."
}
