//
//  UserDefined.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 01/12/2021.
//

import Foundation

class UserDefined {
    static var backendURLString = "http://222.252.19.88"
    static var backendPORT = UInt16(5000)
    static var populateID = "execute.ID"
    static var populateRuleID = "execute.ID,input.condition.compare.ID"
}
