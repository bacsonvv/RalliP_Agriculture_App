//
//  RuleCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 23/12/2021.
//

import UIKit

protocol RuleCellDelegate: AnyObject {
    func removeCell(index: Int)
    func ruleActiveDidChange(state: Bool, index: Int)
}

class RuleCell: BaseTBCell {
    @IBOutlet weak var lblRuleName: UILabel!
    @IBOutlet weak var lblRuleStatus: UILabel!
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblRuleTime: UILabel!
    @IBOutlet weak var lblRuleLogic: UILabel!
    
    var cellIndex = -1
    weak var delegate: RuleCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            containerView.backgroundColor = UIColor.colorFromHexString(hex: "#C4D5DF")
        } else {
            containerView.backgroundColor = UIColor.white
        }
    }
    
    func setupData(data: RuleModel, index: Int) {
        cellIndex = index
        
        if let ruleName = data.name {
            lblRuleName.text = ruleName
        }
        if let ruleStatus = data.active {
            if ruleStatus {
                switchState.setOn(true, animated: false)
                lblRuleStatus.text = SwitchState.on.rawValue
            } else {
                switchState.setOn(false, animated: false)
                lblRuleStatus.text = SwitchState.off.rawValue
            }
        }
        
        if let timeStart = data.input.schedule.timeStart, let timeStop = data.input.schedule.timeStop {
            lblRuleTime.text = "\(timeStart) - \(timeStop)"
        }
        
        if let logic = data.input.condition.logic {
            if logic == LogicType.and.rawValue {
                lblRuleLogic.text = LogicTypeText.and.rawValue
            } else {
                lblRuleLogic.text = LogicTypeText.or.rawValue
            }
        }
    }
    
    @IBAction func clickToRemoveCell(_ sender: Any) {
        delegate?.removeCell(index: cellIndex)
    }
    @IBAction func clickToChangeRuleActive(_ sender: Any) {
        if switchState.isOn {
            switchState.setOn(true, animated: true)
            lblRuleStatus.text = SwitchState.on.rawValue
        } else {
            switchState.setOn(false, animated: true)
            lblRuleStatus.text = SwitchState.off.rawValue
        }
        delegate?.ruleActiveDidChange(state: switchState.isOn, index: cellIndex)
    }
}
