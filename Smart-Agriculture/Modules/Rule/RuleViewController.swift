//
//  RuleViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 23/12/2021.
//

import UIKit
import IBAnimatable

class RuleViewController: BaseViewController {
    @IBOutlet weak var btnLeftMenu: UIButton!
    @IBOutlet weak var ruleTableView: UITableView!
    @IBOutlet weak var shadowView: AnimatableView!
    
    private var ruleList = [RuleModel]()
    private var cellHeight: CGFloat = 211
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        // Call api to get list of rule (type = 3) in green house
        getListRuleInGreenhouse()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        if let revealViewController = revealViewController() {
            btnLeftMenu.addTarget(revealViewController, action: #selector(revealViewController.revealSideMenu), for: .touchUpInside)
        }
        
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        ruleTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRuleInGreenhouse()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        RuleCell.registerCellByNib(ruleTableView)
        ruleTableView.delegate = self
        ruleTableView.dataSource = self
    }
}

extension RuleViewController {
    @IBAction func clickToAddRule(_ sender: Any) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.add.rawValue
        addEditRuleViewController.ruleType = RuleType.rule.rawValue
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension RuleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ruleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = RuleCell.loadCell(tableView) as? RuleCell else { return UITableViewCell() }
        cell.delegate = self
        cell.setupData(data: ruleList[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.edit.rawValue
        addEditRuleViewController.ruleType = RuleType.rule.rawValue
        addEditRuleViewController.parentController = self
        addEditRuleViewController.data = ruleList[indexPath.row]
        addEditRuleViewController.dataPutWithOutputDevice = ruleList[indexPath.row].clone()
        addEditRuleViewController.dataPutWithOutputScript = ruleList[indexPath.row].clone()
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

// MARK: API Call
extension RuleViewController {
    func getListRuleInGreenhouse() {
        guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
        GreenHouseRouter(endpoint: .getListRule(type: RuleType.rule.rawValue, greenHouseId: currentGreenHouseId)).mapTo([RuleModel].self, success: { [weak self] (listScenario) in
            guard let weakSelf = self else { return }
            weakSelf.ruleList = listScenario
            DispatchQueue.main.async {
                weakSelf.ruleTableView.reloadData()
            }
        })
    }
}

extension RuleViewController: RuleCellDelegate {
    func removeCell(index: Int) {
        let alert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.confirmDeleteMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ConstantText.confirmAlertButtonTitle, style: .default, handler: { [weak self] _ in
            guard let id = self?.ruleList[index].id else { return }
            RuleRouter(endpoint: .deleteRule(id: id)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                guard let weakSelf = self else { return }
                if isSuccess {
                    DispatchQueue.main.async {
                        let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteSuccessMessage, preferredStyle: .alert)
                        finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: { [weak self] _ in
                            guard let weakSelf1 = self else { return }
                            weakSelf1.ruleList.remove(at: index)
                            weakSelf1.ruleTableView.reloadData()
                        }))
                        weakSelf.present(finishAlert, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteFailMessage, preferredStyle: .alert)
                        finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
                        weakSelf.present(finishAlert, animated: true, completion: nil)
                    }
                }
                
            }, failure: { [weak self] _ in
                guard let weakSelf = self else { return }
                DispatchQueue.main.async {
                    let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteFailMessage, preferredStyle: .alert)
                    finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
                    weakSelf.present(finishAlert, animated: true, completion: nil)
                }
            })
        }))
        alert.addAction(UIAlertAction(title: ConstantText.cancelAlertButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ruleActiveDidChange(state: Bool, index: Int) {
        guard let id = ruleList[index].id else { return  }
        ruleList[index].active = state
        RuleRouter(endpoint: .updateSchedule(id: id, data: ruleList[index])).mapTo(Bool.self, success: { [weak self] (isSuccess) in
            guard let weakSelf = self else { return }
            if isSuccess {
                DispatchQueue.main.async {
                    weakSelf.ruleTableView.reloadData()
                }
            }
        })
    }
}
