//
//  ScheduleListCell.swift
//  Smart-Agriculture
//
//  Created by admin on 11/11/2021.
//

import UIKit

protocol ScheduleListCellDelegate {
    func removeCell(index: Int)
    func scheduleActiveDidChange(state: Bool, index: Int)
}

class ScheduleListCell: BaseTBCell {
    @IBOutlet weak var lblScheduleName: UILabel!
    @IBOutlet weak var lblScheduleStatus: UILabel!
    @IBOutlet weak var lblScheduleTime: UILabel!
    @IBOutlet weak var lblScheduleRepeat: UILabel!
    @IBOutlet weak var lblDeviceType: UILabel!
    @IBOutlet weak var lblDeviceTypeValue: UILabel!
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnDeleteCell: UIButton!
    
    var cellIndex = -1
    var delegate: ScheduleListCellDelegate?
    var deviceType = DeviceType.relay.rawValue
    
    override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)

         if selected {
             containerView.backgroundColor = UIColor.colorFromHexString(hex: "#C4D5DF")
         } else {
             containerView.backgroundColor = UIColor.white
         }
     }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        switchState.scale(x: 0.75, y: 0.75)
        self.selectionStyle = .none
    }
    
    func setupData(data: RuleModel, index: Int) {
        cellIndex = index
        if let scheduleName = data.name {
            lblScheduleName.text = scheduleName
        }
        if let scheduleStatus = data.active {
            if scheduleStatus {
                switchState.setOn(true, animated: false)
                lblScheduleStatus.text = SwitchState.on.rawValue
            } else {
                switchState.setOn(false, animated: false)
                lblScheduleStatus.text = SwitchState.off.rawValue
            }
        }
        if let scheduleTime = data.input.schedule.timeStart {
            lblScheduleTime.text = "\(scheduleTime)"
        }
        
        var repeatText = [String]()
        if data.input.loop.monday,
           data.input.loop.tuesday,
           data.input.loop.wednesday,
           data.input.loop.thursday,
           data.input.loop.friday,
           data.input.loop.saturday,
           data.input.loop.sunday {
            repeatText.append(RepeatText.everyDay.rawValue)
        } else {
            if data.input.loop.monday {
                repeatText.append(RepeatText.t2.rawValue)
            }
            if data.input.loop.tuesday {
                repeatText.append(RepeatText.t3.rawValue)
            }
            if data.input.loop.wednesday {
                repeatText.append(RepeatText.t4.rawValue)
            }
            if data.input.loop.thursday {
                repeatText.append(RepeatText.t5.rawValue)
            }
            if data.input.loop.friday {
                repeatText.append(RepeatText.t6.rawValue)
            }
            if data.input.loop.saturday {
                repeatText.append(RepeatText.t7.rawValue)
            }
            if data.input.loop.sunday {
                repeatText.append(RepeatText.cn.rawValue)
            }
            if !data.input.loop.monday,
               !data.input.loop.tuesday,
               !data.input.loop.wednesday,
               !data.input.loop.thursday,
               !data.input.loop.friday,
               !data.input.loop.saturday,
               !data.input.loop.sunday {
                lblScheduleRepeat.text = ConstantText.undefinedText
            }
        }
        if repeatText.count > 1 {
            lblScheduleRepeat.text = repeatText.joined(separator: ",")
        } else {
            if repeatText.count == 1 {
                lblScheduleRepeat.text = repeatText[0]
            }
        }
        if data.execute.isNotEmpty {
            if let pull = data.execute[0].pull {
                deviceType = DeviceType.relay.rawValue
                lblDeviceType.text = DeviceTypeName.pump.rawValue
                if pull == OnOffState.on.rawValue {
                    lblDeviceTypeValue.text = SwitchState.on.rawValue
                } else {
                    lblDeviceTypeValue.text = SwitchState.off.rawValue
                }
            }
            if let dim = data.execute[0].dim, let cct = data.execute[0].cct {
                deviceType = DeviceType.led.rawValue
                lblDeviceType.text = DeviceTypeName.light.rawValue
                lblDeviceTypeValue.text = "\(DevicePropertyName.dim.rawValue): \(dim) & \(DevicePropertyName.cct.rawValue): \(cct)"
            }
        }
        if let scheduleStatus = data.active {
            if scheduleStatus {
                if deviceType == DeviceType.led.rawValue {
                    lblDeviceType.isHidden = false
                    lblDeviceTypeValue.isHidden = false
                }
            } else {
                if deviceType == DeviceType.led.rawValue {
                    lblDeviceType.isHidden = true
                    lblDeviceTypeValue.isHidden = true
                }
            }
        }
    }
}

extension ScheduleListCell {
    @IBAction func clickToDeleteCell(_ sender: Any) {
        delegate?.removeCell(index: cellIndex)
    }

    @IBAction func clickToChangeScheduleActive(_ sender: Any) {
        if switchState.isOn {
            if deviceType == DeviceType.led.rawValue {
                lblDeviceType.isHidden = false
                lblDeviceTypeValue.isHidden = false
            }
            switchState.setOn(true, animated: true)
            lblScheduleStatus.text = SwitchState.on.rawValue
        } else {
            if deviceType == DeviceType.led.rawValue {
                lblDeviceType.isHidden = true
                lblDeviceTypeValue.isHidden = true
            }
            switchState.setOn(false, animated: true)
            lblScheduleStatus.text = SwitchState.off.rawValue
        }
        delegate?.scheduleActiveDidChange(state: switchState.isOn, index: cellIndex)
    }
}
