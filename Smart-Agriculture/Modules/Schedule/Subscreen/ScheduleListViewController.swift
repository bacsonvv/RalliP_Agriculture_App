//
//  ScheduleListViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 12/11/2021.
//

import UIKit
import IBAnimatable

class ScheduleListViewController: BaseViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnAddSchedule: UIButton!
    @IBOutlet weak var scheduleTableView: UITableView!
    @IBOutlet weak var shadowView: AnimatableView!
    
    private var cellHeight: CGFloat = 271
    var type = DeviceType.relay.rawValue
    var scheduleList = [RuleModel]()
    var relay = RelayModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Call api to get list of schedule of a relay
        getListScheduleOfRelay()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        lblBaseScreenTitle.text = "Lịch: \(relay.name ?? "")"
        
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        scheduleTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListScheduleOfRelay()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        ScheduleListCell.registerCellByNib(scheduleTableView)
        scheduleTableView.delegate = self
        scheduleTableView.dataSource = self
    }
}

// MARK: Handle IBActions
extension ScheduleListViewController {
    @IBAction func clickToAddSchedule(_ sender: Any) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.add.rawValue
        addEditRuleViewController.deviceType = type
        addEditRuleViewController.ruleType = RuleType.schedule.rawValue
        addEditRuleViewController.relay = relay
        addEditRuleViewController.parentController = self
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension ScheduleListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scheduleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = ScheduleListCell.loadCell(tableView) as? ScheduleListCell else { return UITableViewCell() }
        cell.setupData(data: scheduleList[indexPath.row], index: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.edit.rawValue
        addEditRuleViewController.ruleType = RuleType.schedule.rawValue
        addEditRuleViewController.parentController = self
        if scheduleList[indexPath.row].execute.isNotEmpty {
            guard let executeRelay = scheduleList[indexPath.row].execute[0].id as? RelayModel else { return }
            if let type = executeRelay.type {
                if type == DeviceType.relay.rawValue {
                    addEditRuleViewController.deviceType = DeviceType.relay.rawValue
                } else if type == DeviceType.led.rawValue {
                    addEditRuleViewController.deviceType = DeviceType.led.rawValue
                }
            }
        }
        addEditRuleViewController.data = scheduleList[indexPath.row]
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
}

// MARK: ScheduleListCellDelegate
extension ScheduleListViewController: ScheduleListCellDelegate {
    func scheduleActiveDidChange(state: Bool, index: Int) {
        guard let id = scheduleList[index].id else { return  }
        scheduleList[index].active = state
        RuleRouter(endpoint: .updateSchedule(id: id, data: scheduleList[index])).mapTo(Bool.self, success: { [weak self] (isSuccess) in
            guard let weakSelf = self else { return }
            if isSuccess {
                DispatchQueue.main.async {
                    weakSelf.scheduleTableView.reloadData()
                }
            }
        })
    }

    func removeCell(index: Int) {
        let alert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.confirmDeleteMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ConstantText.confirmAlertButtonTitle, style: .default, handler: { [weak self] _ in
            guard let id = self?.scheduleList[index].id else { return }
            RuleRouter(endpoint: .deleteRule(id: id)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                guard let weakSelf = self else { return }
                if isSuccess {
                    DispatchQueue.main.async {
                        let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteSuccessMessage, preferredStyle: .alert)
                        finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: { [weak self] _ in
                            guard let weakSelf1 = self else { return }
                            weakSelf1.scheduleList.remove(at: index)
                            weakSelf1.scheduleTableView.reloadData()
                        }))
                        weakSelf.present(finishAlert, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteFailMessage, preferredStyle: .alert)
                        finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
                        weakSelf.present(finishAlert, animated: true, completion: nil)
                    }
                }
                
            }, failure: { [weak self] _ in
                guard let weakSelf = self else { return }
                DispatchQueue.main.async {
                    let finishAlert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.deleteFailMessage, preferredStyle: .alert)
                    finishAlert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
                    weakSelf.present(finishAlert, animated: true, completion: nil)
                }
            })
        }))
        alert.addAction(UIAlertAction(title: ConstantText.cancelAlertButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: Override functions
extension ScheduleListViewController {
    override func goBackToPreviousViewController() {
        guard let navigationController = self.navigationController else { return }
        navigationController.popViewController(animated: true)
    }
}

// MARK: API Call
extension ScheduleListViewController {
    func getListScheduleOfRelay() {
        if let executeId = relay.id {
            RuleRouter(endpoint: .getListScheduleOfDevice(type: RuleType.schedule.rawValue, executeId: executeId)).mapTo([RuleModel].self, success: { [weak self] listRule in
                guard let weakSelf = self else { return }
                weakSelf.scheduleList = listRule
                DispatchQueue.main.async {
                    weakSelf.scheduleTableView.reloadData()
                }
            })
        }
    }
}
