//
//  ScheduleCell.swift
//  Smart-Agriculture
//
//  Created by admin on 09/11/2021.
//

import UIKit
import Kingfisher

class ScheduleCell: BaseCLCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var lblDeviceState: UILabel!
    
    private var dimValue = 1
    private var cctValue = 1
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                containerView.backgroundColor = UIColor.colorFromHexString(hex: "#C4D5DF")
            } else {
                containerView.backgroundColor = .white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
    }

    func setupData(data: RelayModel) {
        if let deviceName = data.name {
            lblDeviceName.text = deviceName
        }
        if let avatar = data.avatar {
            let url = URL(string: avatar)
            deviceImage.kf.indicatorType = .activity
            deviceImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if let type = data.type {
            if type == DeviceType.relay.rawValue {
                if let status = data.status {
                    if status == 1 {
                        lblDeviceState.text = SwitchState.on.rawValue
                    } else {
                        lblDeviceState.text = SwitchState.off.rawValue
                    }
                }
            } else {
                for property in data.value {
                    if property.name == DevicePropertyName.dim.rawValue {
                        dimValue = property.value ?? 1
                    } else {
                        cctValue = property.value ?? 1
                    }
                }
                lblDeviceState.text = "\(DevicePropertyName.dim.rawValue): \(dimValue) & \(DevicePropertyName.cct.rawValue): \(cctValue)"
            }
        }
    }
}
