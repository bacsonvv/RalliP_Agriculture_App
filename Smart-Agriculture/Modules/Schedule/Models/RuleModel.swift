//
//  RuleModel.swift
//  Smart-Agriculture
//
//  Created by admin on 15/11/2021.
//

import SwiftyJSON

class RuleModel: BaseModel {
    var name: String?
    var greenhouseId: String?
    var active: Bool?
    var type: Int?
    var input: InputProperty = InputProperty()
    var execute: [Execute] = [Execute]()
    var mac: String?
    var nLed: Int?
    var nRelay: Int?
    var createdAt: String?
    var id: String?
    var temp = false
    
    override func mapping(fromJSON json: JSON) {
        name = json["name"].string
        greenhouseId = json["greenhouseId"].string
        active = json["active"].bool
        type = json["type"].int
        input = InputProperty(fromJSON: json["input"]) ?? InputProperty()
        execute = [Execute](fromJSON: json["execute"])
        mac = json["mac"].string
        nLed = json["nLed"].int
        nRelay = json["nRelay"].int
        createdAt = json["createdAt"].string
        id = json["id"].string
    }

    func clone() -> RuleModel {
        let ruleModel = RuleModel()
        ruleModel.name = name
        ruleModel.greenhouseId = greenhouseId
        ruleModel.active = active
        ruleModel.type = type
        ruleModel.input = input
        ruleModel.execute = execute
        ruleModel.mac = mac
        ruleModel.nLed = nLed
        ruleModel.nRelay = nRelay
        ruleModel.createdAt = createdAt
        ruleModel.id = id

        return ruleModel
    }
}

class InputProperty: BaseModel {
    var schedule: ScheduleInRule = ScheduleInRule()
    var loop: DayLoop = DayLoop()
    var condition: RuleCondition = RuleCondition()
    
    override func mapping(fromJSON json: JSON) {
        schedule = ScheduleInRule(fromJSON: json["schedule"]) ?? ScheduleInRule()
        loop = DayLoop(fromJSON: json["loop"]) ?? DayLoop()
        condition = RuleCondition(fromJSON: json["condition"]) ?? RuleCondition()
    }
}

class ScheduleInRule: BaseModel {
    var timeStart: String?
    var timeStop: String?
    var timeUpdate: Int?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        timeStart = json["timeStart"].string
        timeStop = json["timeStop"].string
        timeUpdate = json["timeUpdate"].int
    }
}

class DayLoop: BaseModel {
    var monday: Bool = false
    var tuesday: Bool = false
    var wednesday: Bool = false
    var thursday: Bool = false
    var friday: Bool = false
    var saturday: Bool = false
    var sunday: Bool = false
    
    override func mapping(fromJSON json: JSON) {
        monday = json["monday"].bool ?? false
        tuesday = json["tuesday"].bool ?? false
        wednesday = json["wednesday"].bool ?? false
        thursday = json["thursday"].bool ?? false
        friday = json["friday"].bool ?? false
        saturday = json["saturday"].bool ?? false
        sunday = json["sunday"].bool ?? false
    }
}

class RuleCondition: BaseModel {
    var compare: [CompareInCondition] = [CompareInCondition]()
    var logic: String?
    
    override func mapping(fromJSON json: JSON) {
        compare = [CompareInCondition](fromJSON: json["compare"])
        logic = json["logic"].string
    }
}

class  CompareInCondition: BaseModel {
    var id: SensorModel = SensorModel()
    var value: Double?
    var equal: String?
    
    override func mapping(fromJSON json: JSON) {
        id = SensorModel(fromJSON: json["ID"]) ?? SensorModel()
        value = json["value"].double
        equal = json["equal"].string
    }
}

class Execute: BaseModel {
    var id: Any?
    var type: String?
    var delay: Int?
    var pull: Int?
    var dim: Int?
    var cct: Int?
    var during: Int?
    
    override func mapping(fromJSON json: JSON) {
        type = json["type"].string
        delay = json["delay"].int
        pull = json["pull"].int
        dim = json["DIM"].int
        cct = json["CCT"].int
        during = json["during"].int
        if let type = json["type"].string {
            switch type {
            case ExecuteType.relay.rawValue:
                id = RelayModel(fromJSON: json["ID"]) ?? RelayModel()
            case ExecuteType.script.rawValue:
                id = RuleModel(fromJSON: json["ID"]) ?? RuleModel()
            default:
                break
            }
        }
    }
}
