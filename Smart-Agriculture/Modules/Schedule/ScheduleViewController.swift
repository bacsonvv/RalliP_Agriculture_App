//
//  ScheduleViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 08/11/2021.
//

import UIKit
import IBAnimatable
import PagingKit

class ScheduleViewController: BaseViewController {
    @IBOutlet weak var btnLeftMenu: UIButton!
    @IBOutlet weak var shadowView: AnimatableView!
    @IBOutlet weak var deviceCollectionView: UICollectionView!

    private var deviceList: [RelayModel] = [RelayModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        if let revealViewController = revealViewController() {
            btnLeftMenu.addTarget(revealViewController, action: #selector(revealViewController.revealSideMenu), for: .touchUpInside)
        }
        
        // Call api to get list of relay in green house
        getListRelay()
        setupPullToRefresh()
        setupCollectionView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        deviceCollectionView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRelay()
        self.refreshControl.endRefreshing()
    }
    
    func setupCollectionView() {
        ScheduleCell.registerCellByNib(deviceCollectionView)
        deviceCollectionView.delegate = self
        deviceCollectionView.dataSource = self
        if let flowLayout = deviceCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
        }
    }
}

// MARK: UICollectionViewDelegate
extension ScheduleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deviceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = ScheduleCell.loadCell(collectionView, path: indexPath) as? ScheduleCell else { return UICollectionViewCell() }
        cell.setupData(data: deviceList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow: CGFloat = 2
        let spacingBetweenCells: CGFloat = 10

        let totalSpacing = ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        let width = ((UIScreen.main.bounds.width - totalSpacing) / numberOfItemsPerRow)
    
        return CGSize(width: width, height: 234)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 10)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let scheduleListViewController = R.storyboard.scheduleList.scheduleListViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        scheduleListViewController.parentController = self
        scheduleListViewController.relay = deviceList[indexPath.row]
        scheduleListViewController.type = deviceList[indexPath.row].type ?? 0
        navigationController.pushViewController(scheduleListViewController, animated: true)
    }
}

// MARK: API Call
extension ScheduleViewController {
    func getListRelay() {
        guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
        GreenHouseRouter(endpoint: .getListRelayAndLed(greenHouseId: currentGreenHouseId, type: -1)).mapTo([RelayModel].self, success: { [weak self] (listRelay) in
            guard let weakSelf = self else { return }
            weakSelf.deviceList = listRelay
            weakSelf.deviceCollectionView.reloadData()
        })
    }
}
