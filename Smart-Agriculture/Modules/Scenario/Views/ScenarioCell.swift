//
//  ScenarioCell.swift
//  Smart-Agriculture
//
//  Created by admin on 17/11/2021.
//

import UIKit

protocol ScenarioCellDelegate {
    func removeCell(index: Int)
    func scriptActiveDidChange(state: Bool, index: Int)
}

class ScenarioCell: BaseTBCell {
    @IBOutlet weak var lblScenarioName: UILabel!
    @IBOutlet weak var lblScenarioState: UILabel!
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnDeleteCell: UIButton!
    @IBOutlet weak var lblNumberOfDevice: UILabel!
    @IBOutlet weak var lblNumberOfRelay: UILabel!
    @IBOutlet weak var lblNumberOfLed: UILabel!
    
    var cellIndex = -1
    var delegate: ScenarioCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            containerView.backgroundColor = UIColor.colorFromHexString(hex: "#C4D5DF")
        } else {
            containerView.backgroundColor = UIColor.white
        }
    }
    
    func setupData(data: RuleModel, index: Int) {
        cellIndex = index
        if let scenarioName = data.name {
            lblScenarioName.text = scenarioName
        }
        if let scenarioStatus = data.active {
            if scenarioStatus {
                switchState.setOn(true, animated: false)
                lblScenarioState.text = SwitchState.on.rawValue
            } else {
                switchState.setOn(false, animated: false)
                lblScenarioState.text = SwitchState.off.rawValue
            }
        }
        
        let numberOfDevice = data.execute.count
        lblNumberOfDevice.text = "\(numberOfDevice)"
        
        var numberOfRelay = 0
        var numberOfLed = 0
        for device in data.execute {
            guard let deviceInExcute = device.id as? RelayModel else { return }
            if deviceInExcute.type == DeviceType.relay.rawValue {
                numberOfRelay += 1
            } else {
                numberOfLed += 1
            }
        }
        
        lblNumberOfRelay.text = "\(numberOfRelay)"
        lblNumberOfLed.text = "\(numberOfLed)"
    }
}

extension ScenarioCell {
    @IBAction func clickToDeleteCell(_ sender: Any) {
        delegate?.removeCell(index: cellIndex)
    }
    @IBAction func clickToChangeState(_ sender: Any) {
        if switchState.isOn {
            switchState.setOn(true, animated: true)
            lblScenarioState.text = SwitchState.on.rawValue
        } else {
            switchState.setOn(false, animated: true)
            lblScenarioState.text = SwitchState.off.rawValue
        }
        
        delegate?.scriptActiveDidChange(state: switchState.isOn, index: cellIndex)
    }
}
