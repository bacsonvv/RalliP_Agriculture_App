//
//  GreenHouseViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 29/10/2021.
//

import UIKit
import PagingKit
import IBAnimatable

class GreenHouseViewController: BaseViewController {
    @IBOutlet weak var shadowView: AnimatableView!
    @IBOutlet weak var btnLeftMenu: UIButton!
    
    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    let menuTitle = [GreenHouseMenuTitle.sensor.rawValue, GreenHouseMenuTitle.relay.rawValue, GreenHouseMenuTitle.led.rawValue]
    var selectedPage = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        if let revealViewController = revealViewController() {
            btnLeftMenu.addTarget(revealViewController, action: #selector(revealViewController.revealSideMenu), for: .touchUpInside)
        }
        
        menuViewController.register(nib: UINib(nibName: "GreenHouseMenuCell", bundle: nil), forCellWithReuseIdentifier: "GreenHouseMenuCell")
        menuViewController.registerFocusView(nib: UINib(nibName: "FocusView", bundle: nil))
        menuViewController.reloadData()
        contentViewController.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PagingContentViewController {
            contentViewController = viewController
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
        } else if let viewController = segue.destination as? PagingMenuViewController {
            menuViewController = viewController
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
        }
    }
}

// MARK: PagingContentViewControllerDataSource
extension GreenHouseViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return menuTitle.count
    }

    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        switch index {
        case 0:
            guard let sensorViewController = R.storyboard.sensor.sensorViewController() else { return UIViewController() }
            return sensorViewController
        case 1:
            guard let relayViewController = R.storyboard.relay.relayViewController() else { return UIViewController() }
            return relayViewController
        case 2:
            guard let ledViewController = R.storyboard.led.ledViewController() else { return UIViewController() }
            return ledViewController
        default:
            return UIViewController()
        }
    }
}

// MARK: PagingContentViewControllerDelegate
extension GreenHouseViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController?.scroll(index: index, percent: percent, animated: false)
        if percent == 0 {
            if selectedPage != index {
                selectedPage = index
            }
        }
    }
}

// MARK: PagingMenuViewControllerDataSource
extension GreenHouseViewController: PagingMenuViewControllerDataSource {
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return menuTitle.count
    }

    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return UIScreen.main.bounds.size.width / CGFloat(menuTitle.count)
    }

    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "GreenHouseMenuCell", for: index)
        if let greenHouseMenucell = cell as? GreenHouseMenuCell {
            greenHouseMenucell.setupData(data: menuTitle[index])
            greenHouseMenucell.focus(width: UIScreen.main.bounds.size.width / CGFloat(menuTitle.count))
        }
        return cell
    }
}

// MARK: PagingMenuViewControllerDelegate
extension GreenHouseViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        if selectedPage != page {
            selectedPage = page
            contentViewController?.scroll(to: page, animated: true)
        }
    }
}
