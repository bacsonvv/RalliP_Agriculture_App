//
//  GreenHouseCell.swift
//  Smart-Agriculture
//
//  Created by admin on 05/11/2021.
//

import UIKit

class GreenHouseCell: BaseTBCell {
    @IBOutlet weak var lblGreenHouseName: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }
    
    func setupData(data: GreenHouseModel) {
        if let greenHouseName = data.name {
            lblGreenHouseName.text = greenHouseName
        }
    }
}
