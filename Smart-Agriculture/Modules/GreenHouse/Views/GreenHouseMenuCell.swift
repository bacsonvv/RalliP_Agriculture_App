//
//  GreenHouseMenuCell.swift
//  Smart-Agriculture
//
//  Created by admin on 29/10/2021.
//

import UIKit
import PagingKit
import SnapKit

class GreenHouseMenuCell: PagingMenuViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.snp.makeConstraints { (make) in
            make.width.equalTo(320)
        }
    }
    
    func setupData(data: String) {
        menuTitle.text = data
    }
    
    override public var isSelected: Bool {
        didSet {
            if isSelected {
                menuTitle.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            } else {
                menuTitle.font = UIFont.systemFont(ofSize: 15, weight: .regular)
            }
        }
    }
}

// MARK: Public methods
extension GreenHouseMenuCell {
    func focus(width: CGFloat) {
        containerView.snp.updateConstraints { (make) in
            make.width.equalTo(width)
        }
        containerView.superview?.layoutIfNeeded()
    }
}
