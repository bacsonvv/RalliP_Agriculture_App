//
//  RelayModel.swift
//  Smart-Agriculture
//
//  Created by admin on 10/11/2021.
//

import SwiftyJSON

class RelayModel: BaseModel {
    var active: Bool?
    var timeUpdate: Int?
    var avatar: String?
    var name: String?
    var greenhouseId: String?
    var mac: String?
    var status: Int?
    var value: [SubRelayModel] = [SubRelayModel]()
    var type: Int?
    var id: String?
    var createdAt: String?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        active = json["active"].bool
        timeUpdate = json["timeUpdate"].int
        avatar = json["avatar"].string
        name = json["name"].string
        greenhouseId = json["greenhouseId"].string
        mac = json["mac"].string
        status = json["status"].int
        value = [SubRelayModel](fromJSON: json["values"])
        type = json["type"].int
        id = json["id"].string
        createdAt = json["createdAt"].string
    }
}

class SubRelayModel: BaseModel {
    var name: String?
    var value: Int?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        name = json["name"].string
        value = json["value"].int
    }
}
