//
//  SensorModel.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 01/12/2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class SensorModel: BaseModel {
    var active: Bool?
    var timeUpdate: Int?
    var avatar: String?
    var name: String?
    var greenHouseId: String?
    var mac: String?
    var type: Int?
    var createdAt: String?
    var updatedAt: String?
    var battery: Int?
    var curValue: Double?
    var max: Int?
    var min: Int?
    var paramA: Int?
    var paramB: Int?
    var paramC: Int?
    var id: String?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        
        active = json["active"].bool
        timeUpdate = json["timeUpdate"].int
        avatar = json["avatar"].string
        name = json["name"].string
        greenHouseId = json["greenhouseId"].string
        mac = json["mac"].string
        type = json["type"].int
        createdAt = json["createdAt"].string
        updatedAt = json["updatedAt"].string
        battery = json["battery"].int
        curValue = json["curValue"].double
        max = json["max"].int
        min = json["min"].int
        paramA = json["paramA"].int
        paramB = json["paramB"].int
        paramC = json["paramC"].int
        id = json["id"].string
    }
}
