//
//  GreenHouseModel.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 01/12/2021.
//

import Foundation
import SwiftyJSON

class GreenHouseModel: BaseModel {
    var avatar: String?
    var active: Bool?
    var name: String?
    var mac: String?
    var createdAt: String?
    var id: String?
    
    static var shared = GreenHouseModel()
    var currentGreenHouse: GreenHouseModel?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        avatar = json["avatar"].string
        active = json["active"].bool
        name = json["name"].string
        mac = json["mac"].string
        createdAt = json["createdAt"].string
        id = json["id"].string
    }
}

extension GreenHouseModel {
    func saveGreenHouse(_ model: GreenHouseModel) {
        currentGreenHouse = model
    }
}
