//
//  DownloadDataViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 14/01/2022.
//

import UIKit
import Kingfisher

class DownloadDataViewController: BaseViewController {
    @IBOutlet weak var lblSensorName: UILabel!
    @IBOutlet weak var lblSensorValue: UILabel!
    @IBOutlet weak var lblBatteryValue: UILabel!
    @IBOutlet weak var sensorImage: UIImageView!
    @IBOutlet weak var tfDownloadDate: UITextField!
    @IBOutlet weak var btnSelectOne: UIButton!
    @IBOutlet weak var btnSelectAll: UIButton!
    @IBOutlet weak var download1DayView: UIView!
    @IBOutlet weak var download7DayView: UIView!
    @IBOutlet weak var download30DayView: UIView!
    
    private var isSelectOne = true
    let timePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var date = Date()
    let calendar = Calendar.current
    var sensor = SensorModel()
    private var checkImage = UIImage(named: "ic-check-radio")?.withRenderingMode(.alwaysOriginal)
    private var uncheckImage = UIImage(named: "ic-uncheck-radio")?.withRenderingMode(.alwaysOriginal)
    var finalDate = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        setupView(views: [download1DayView, download7DayView, download30DayView])
        createTimePicker()
    }
    
    func setupData() {
        var sensorValue = 0.0
        if let sensorValueTmp = sensor.curValue {
            sensorValue = sensorValueTmp
        }
        if let sensorName = sensor.name {
            lblSensorName.text = sensorName
        }
        if let battery = sensor.battery {
            lblBatteryValue.text = "\(battery)%"
        }
        if let sensorType = sensor.type {
            switch sensorType {
            case SensorType.temperature.rawValue:
                lblSensorValue.text = "\(sensorValue) \(SensorUnitType.temperature.rawValue)"
            case SensorType.soil.rawValue:
                lblSensorValue.text = "\(sensorValue) \(SensorUnitType.soilAndHumidity.rawValue)"
            case SensorType.light.rawValue:
                lblSensorValue.text = "\(sensorValue) \(SensorUnitType.light.rawValue)"
            case SensorType.humidity.rawValue:
                lblSensorValue.text = "\(sensorValue) \(SensorUnitType.soilAndHumidity.rawValue)"
            case SensorType.co2.rawValue:
                lblSensorValue.text = "\(sensorValue) \(SensorUnitType.co2.rawValue)"
            default:
                break
            }
        }
        if let sensorImg = sensor.avatar {
            let url = URL(string: sensorImg)
            sensorImage.kf.indicatorType = .activity
            sensorImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        
        dateFormatter.dateFormat = "YYYY-MM-dd"
        finalDate = dateFormatter.string(from: date)
    }
    
    func setupView(views: [UIView]) {
        dateFormatter.dateFormat = "dd/MM/YYYY"
        tfDownloadDate.text = dateFormatter.string(from: date)
        
        isSelectOne = true
        btnSelectOne.setImage(checkImage, for: .normal)
        btnSelectAll.setImage(uncheckImage, for: .normal)
        
        views.forEach { view in
            view.layer.cornerRadius = view.bounds.size.height / 2
        }
        
        tfDownloadDate.delegate = self
    }
    
    func createTimePicker() {
        timePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            timePicker.preferredDatePickerStyle = .wheels
        }
        
        // Toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // Bar button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(timePickingDone))
        toolbar.setItems([doneButton], animated: true)
        
        // Assign toolbar
        tfDownloadDate.inputAccessoryView = toolbar
        
        // Assign time picker to the text field
        tfDownloadDate.inputView = timePicker
    }
    
    @objc func timePickingDone() {
        date = timePicker.date
        
        let day = calendar.component(.day, from: date)
        var dayString = ""
        if day < 10 {
            dayString = "0\(day)"
        } else {
            dayString = "\(day)"
        }
        let month = calendar.component(.month, from: date)
        var monthString = ""
        if month < 10 {
            monthString = "0\(month)"
        } else {
            monthString = "\(month)"
        }
        let year = calendar.component(.year, from: date)

        finalDate = "\(year)-\(monthString)-\(dayString)"
        tfDownloadDate.text = "\(dayString)/\(monthString)/\(year)"

        self.view.endEditing(true)
    }
}

extension DownloadDataViewController {
    @IBAction func clickToSelectOne(_ sender: Any) {
        if !isSelectOne {
            isSelectOne = true
            btnSelectOne.setImage(checkImage, for: .normal)
            btnSelectAll.setImage(uncheckImage, for: .normal)
        }
    }
    
    @IBAction func clickToSelectAll(_ sender: Any) {
        if isSelectOne {
            isSelectOne = false
            btnSelectOne.setImage(uncheckImage, for: .normal)
            btnSelectAll.setImage(checkImage, for: .normal)
        }
    }

    @IBAction func clickToDownload1DayAgo(_ sender: Any) {
        if isSelectOne {
            guard let id = sensor.id else { return }
            openBrowser(nDays: 1, date: finalDate, type: "sensors", id: id)
        } else {
            guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
            openBrowser(nDays: 1, date: finalDate, type: "greenhouses", id: currentGreenHouseId)
        }
    }
    
    @IBAction func clickToDownload7DayAgo(_ sender: Any) {
        if isSelectOne {
            guard let id = sensor.id else { return }
            openBrowser(nDays: 7, date: finalDate, type: "sensors", id: id)
        } else {
            guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
            openBrowser(nDays: 7, date: finalDate, type: "greenhouses", id: currentGreenHouseId)
        }
    }
    
    @IBAction func clickToDownload30DayAgo(_ sender: Any) {
        if isSelectOne {
            guard let id = sensor.id else { return }
            openBrowser(nDays: 30, date: finalDate, type: "sensors", id: id)
        } else {
            guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
            openBrowser(nDays: 30, date: finalDate, type: "greenhouses", id: currentGreenHouseId)
        }
    }
    
    func openBrowser(nDays: Int, date: String, type: String, id: String) {
        let finalURLString = "\(UserDefined.backendURLString):\(UserDefined.backendPORT)/api/\(type)/\(id)/sensor_values/export?nDays=\(nDays)&date=\(date)"
        guard let url = URL(string: finalURLString) else { return }
        UIApplication.shared.open(url)
    }
}

extension DownloadDataViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}
