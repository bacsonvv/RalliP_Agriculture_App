//
//  PumpCell.swift
//  Smart-Agriculture
//
//  Created by admin on 03/11/2021.
//

import UIKit

protocol RelayCellDelegate: AnyObject {
    func toAddScheduleScreen(relay: RelayModel)
    func relayStateDidChange(relay: RelayModel)
}

class RelayCell: BaseTBCell {
    @IBOutlet weak var lblRelayName: UILabel!
    @IBOutlet weak var relayImg: UIImageView!
    @IBOutlet weak var lblRelayState: UILabel!
    @IBOutlet weak var switchOnOff: UISwitch!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnSetSchedule: UIButton!
    
    private var relay = RelayModel()
    weak var delegate: RelayCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        self.roundedCellCorner(view: containerView)
        switchOnOff.scale(x: 0.75, y: 0.75)
        btnSetSchedule.customize(backgroundColor: UIColor.colorFromHexString(hex: "#2C698D"), tintColor: .white, cornerRadius: 12)
    }
    
    func setupData(data: RelayModel) {
        self.relay = data
        
        if let relayName = data.name {
            lblRelayName.text = relayName
        }
        if let relayImgString = data.avatar {
            let url = URL(string: relayImgString)
            relayImg.kf.indicatorType = .activity
            relayImg.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if let relayState = data.status {
            if relayState == OnOffState.on.rawValue {
                switchOnOff.setOn(true, animated: false)
                lblRelayState.text = SwitchState.on.rawValue
            } else {
                switchOnOff.setOn(false, animated: false)
                lblRelayState.text = SwitchState.off.rawValue
            }
        }
    }
}

// MARK: HANDLE BUTTON ACTIONS
extension RelayCell {
    @IBAction func clickToChangeState(_ sender: Any) {
        if switchOnOff.isOn {
            self.relay.status = OnOffState.on.rawValue
            lblRelayState.text = SwitchState.on.rawValue
        } else {
            self.relay.status = OnOffState.off.rawValue
            lblRelayState.text = SwitchState.off.rawValue
        }
        delegate?.relayStateDidChange(relay: self.relay)
    }
    
    @IBAction func clickToAddSchedule(_ sender: Any) {
        delegate?.toAddScheduleScreen(relay: self.relay)
    }
}
