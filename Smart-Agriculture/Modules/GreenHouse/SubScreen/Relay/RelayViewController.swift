//
//  PumpViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 03/11/2021.
//

import UIKit

class RelayViewController: BaseViewController {
    @IBOutlet weak var relayTableView: UITableView!
    
    private var cellHeight: CGFloat = 245
    
    var currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    var listRelay = [RelayModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call api to get list of relay in green house
        getListRelay()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        relayTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRelay()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        RelayCell.registerCellByNib(relayTableView)
        relayTableView.delegate = self
        relayTableView.dataSource = self
    }
}

extension RelayViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listRelay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = RelayCell.loadCell(tableView) as? RelayCell else { return UITableViewCell() }
        cell.delegate = self
        cell.setupData(data: listRelay[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

// MARK: API Call
extension RelayViewController {
    func getListRelay() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListRelayAndLed(greenHouseId: greenHouseId, type: DeviceType.relay.rawValue)).mapTo([RelayModel].self, success: { [weak self] (listRelay) in
                guard let weakSelf = self else { return }
                weakSelf.listRelay = listRelay
                weakSelf.relayTableView.reloadData()
            })
        }
    }
}

// MARK: CELL DELEGATE
extension RelayViewController: RelayCellDelegate {
    func relayStateDidChange(relay: RelayModel) {
        RelayRouter(endpoint: .updateRelay(relay: relay)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
            guard let weakSelf = self else { return }
            if isSuccess {
                DispatchQueue.main.async {
                    weakSelf.relayTableView.reloadData()
                }
            }
        })
    }
    
    func toAddScheduleScreen(relay: RelayModel) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.add.rawValue
        addEditRuleViewController.deviceType = DeviceType.relay.rawValue
        addEditRuleViewController.ruleType = RuleType.schedule.rawValue
        addEditRuleViewController.relay = relay
        addEditRuleViewController.parentController = self
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
}
