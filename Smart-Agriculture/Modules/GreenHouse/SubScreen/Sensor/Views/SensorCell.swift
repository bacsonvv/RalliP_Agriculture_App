//
//  SensorCell.swift
//  Smart-Agriculture
//
//  Created by admin on 29/10/2021.
//

import UIKit
import Kingfisher

class SensorCell: BaseTBCell {
    @IBOutlet weak var lblSensorName: UILabel!
    @IBOutlet weak var lblSensorState: UILabel!
    @IBOutlet weak var lblSensorBatteryAmount: UILabel!
    @IBOutlet weak var sensorBatteryImg: UIImageView!
    @IBOutlet weak var sensorImg: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblSensorUnit: UILabel!
    @IBOutlet weak var lblSensorDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupView() {
        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }
    
    func setupData(data: SensorModel) {
        if let sensorName = data.name {
            lblSensorName.text = sensorName
        }
        if let sensorBattery = data.battery {
            lblSensorBatteryAmount.text = "\(sensorBattery)%"
        }
        if let stateValue = data.curValue {
            lblSensorState.text = "\(stateValue)"
        }
        if let sensorImage = data.avatar {
            let url = URL(string: sensorImage)
            sensorImg.kf.indicatorType = .activity
            sensorImg.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        switch data.type {
        case SensorType.temperature.rawValue:
            lblSensorUnit.text = SensorUnitType.temperature.rawValue
        case SensorType.light.rawValue:
            lblSensorUnit.text = SensorUnitType.light.rawValue
        case SensorType.humidity.rawValue:
            lblSensorUnit.text = SensorUnitType.soilAndHumidity.rawValue
        case SensorType.soil.rawValue:
            lblSensorUnit.text = SensorUnitType.soilAndHumidity.rawValue
        default:
            lblSensorUnit.text = SensorUnitType.co2.rawValue
        }
        if let updateDate = data.updatedAt {
            let date = updateDate.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", outGoingFormat: "dd/MM/YYYY HH:mm:ss")
            let dateAndTimeComponents = date.split(separator: " ")
            let dateComponents = dateAndTimeComponents[0].split(separator: "/")
            let day = dateComponents[0]
            let month = dateComponents[1]
            let year = dateComponents[2]
            
            lblSensorDate.text = "\(day) thg \(month) \(year), \(dateAndTimeComponents[1])"
        }
    }
}
