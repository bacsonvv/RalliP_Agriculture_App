//
//  SensorViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 29/10/2021.
//

import UIKit

class SensorViewController: BaseViewController {
    @IBOutlet weak var sensorTableView: UITableView!
    
    private var cellHeight: CGFloat = 200
    
    var currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    var listSensor = [SensorModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call api to get list of sensor in green house
        getListSensor()
        
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        sensorTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListSensor()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        SensorCell.registerCellByNib(sensorTableView)
        sensorTableView.delegate = self
        sensorTableView.dataSource = self
    }
}

extension SensorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSensor.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = SensorCell.loadCell(tableView) as? SensorCell else { return UITableViewCell() }
        cell.setupData(data: listSensor[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let downloadDataViewController = R.storyboard.downloadData.downloadDataViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        downloadDataViewController.sensor = listSensor[indexPath.row]
        navigationController.pushViewController(downloadDataViewController, animated: true)
    }
}

// MARK: API Call
extension SensorViewController {
    func getListSensor() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListSensor(greenHouseId: greenHouseId)).mapTo([SensorModel].self, success: { [weak self] (listSensor) in
                guard let weakSelf = self else { return }
                weakSelf.listSensor = listSensor
                weakSelf.sensorTableView.reloadData()
            })
        }
    }
}
