//
//  LightCell.swift
//  Smart-Agriculture
//
//  Created by admin on 03/11/2021.
//

import UIKit

protocol LedCellDelegate: AnyObject {
    func toAddScheduleScreen(relay: RelayModel)
    func ledValueDidChange(relay: RelayModel)
    func ledStatusDidChange(relay: RelayModel)
}

class LedCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblLedName: UILabel!
    @IBOutlet weak var ledImg: UIImageView!
    @IBOutlet weak var dimSlider: UISlider!
    @IBOutlet weak var dimValue: UITextField!
    @IBOutlet weak var cctSlider: UISlider!
    @IBOutlet weak var cctValue: UITextField!
    @IBOutlet weak var btnSetSchedule: UIButton!
    @IBOutlet weak var lblLedStatus: UILabel!
    @IBOutlet weak var switchOnOff: UISwitch!
    
    weak var delegate: LedCellDelegate?
    private var led = RelayModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
        
        dimValue.isUserInteractionEnabled = false
        cctValue.isUserInteractionEnabled = false
    }
    
    func setupView() {
        self.roundedCellCorner(view: containerView)
        setupSliderValue(sliders: [dimSlider, cctSlider])
        btnSetSchedule.customize(backgroundColor: UIColor.colorFromHexString(hex: "#FFD500"), tintColor: .white, cornerRadius: 12)
    }
    
    func setupSliderValue(sliders: [UISlider]) {
        sliders.forEach { slider in
            slider.minimumValue = 1
            slider.maximumValue = 100
        }
    }
    
    func setupData(data: RelayModel) {
        self.led = data
        
        if let ledName = data.name {
            lblLedName.text = ledName
        }
        if let ledImgString = data.avatar {
            let url = URL(string: ledImgString)
            ledImg.kf.indicatorType = .activity
            ledImg.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if let ledStatus = data.status {
            if ledStatus == OnOffState.on.rawValue {
                switchOnOff.setOn(true, animated: false)
                lblLedStatus.text = SwitchState.on.rawValue
            } else {
                switchOnOff.setOn(false, animated: false)
                lblLedStatus.text = SwitchState.off.rawValue
            }
        }
        if !data.value.contains(where: { $0.name == DevicePropertyName.dim.rawValue }) {
            dimValue.text = "1"
            dimSlider.setValue(1, animated: false)
        }
        if !data.value.contains(where: { $0.name == DevicePropertyName.cct.rawValue }) {
            cctValue.text = "1"
            cctSlider.setValue(1, animated: false)
        }
        for property in data.value {
            if property.name == DevicePropertyName.dim.rawValue {
                if let value = property.value {
                    dimValue.text = "\(value)"
                    dimSlider.setValue(Float(value), animated: false)
                }
            }
            if property.name == DevicePropertyName.cct.rawValue {
                if let value = property.value {
                    cctValue.text = "\(value)"
                    cctSlider.setValue(Float(value), animated: false)
                }
            }
        }
    }
}

// MARK: HANDLE BUTTON ACTIONS
extension LedCell {
    @IBAction func clickChangeLedStatus(_ sender: Any) {
        if switchOnOff.isOn {
            self.led.status = OnOffState.on.rawValue
            lblLedStatus.text = SwitchState.on.rawValue
        } else {
            self.led.status = OnOffState.off.rawValue
            lblLedStatus.text = SwitchState.off.rawValue
        }
        delegate?.ledValueDidChange(relay: self.led)
    }
    
    @IBAction func clickToChangeDIM(_ sender: Any) {
        dimValue.text = String(Int(dimSlider.value))
        for i in 0 ..< led.value.count {
            if led.value[i].name == DevicePropertyName.dim.rawValue {
                led.value[i].value = Int(dimSlider.value)
            }
        }
        delegate?.ledValueDidChange(relay: self.led)
    }
    
    @IBAction func clickToChangeCCT(_ sender: Any) {
        cctValue.text = String(Int(cctSlider.value))
        for i in 0 ..< led.value.count {
            if led.value[i].name == DevicePropertyName.cct.rawValue {
                led.value[i].value = Int(cctSlider.value)
            }
        }
        delegate?.ledValueDidChange(relay: self.led)
    }
    
    @IBAction func clickToAddSchedule(_ sender: Any) {
        delegate?.toAddScheduleScreen(relay: self.led)
    }
}

