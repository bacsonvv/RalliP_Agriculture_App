//
//  LightViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 03/11/2021.
//

import UIKit

class LedViewController: BaseViewController {
    @IBOutlet weak var ledTableView: UITableView!

    private var cellHeight: CGFloat = 350
    private var listLed: [RelayModel] = [RelayModel]()
    var currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call api to get list of led in green house
        getListLed()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        ledTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListLed()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        LedCell.registerCellByNib(ledTableView)
        ledTableView.delegate = self
        ledTableView.dataSource = self
    }
}

extension LedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listLed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = LedCell.loadCell(tableView) as? LedCell else { return UITableViewCell() }
        cell.setupData(data: listLed[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

// MARK: API Call
extension LedViewController {
    func getListLed() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListRelayAndLed(greenHouseId: greenHouseId, type: DeviceType.led.rawValue)).mapTo([RelayModel].self, success: { [weak self] (listRelay) in
                guard let weakSelf = self else { return }
                weakSelf.listLed = listRelay
                weakSelf.ledTableView.reloadData()
            })
        }
    }
}

extension LedViewController: LedCellDelegate {
    func ledStatusDidChange(relay: RelayModel) {
        RelayRouter(endpoint: .updateRelay(relay: relay)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
            guard let weakSelf = self else { return }
            if isSuccess {
                DispatchQueue.main.async {
                    weakSelf.ledTableView.reloadData()
                }
            }
        })
    }
    
    func toAddScheduleScreen(relay: RelayModel) {
        guard let addEditRuleViewController = R.storyboard.addEditRule.addEditRuleViewController() else { return }
        guard let navigationController = self.navigationController else { return }
        addEditRuleViewController.screenType = ScreenType.add.rawValue
        addEditRuleViewController.deviceType = DeviceType.led.rawValue
        addEditRuleViewController.ruleType = RuleType.schedule.rawValue
        addEditRuleViewController.relay = relay
        addEditRuleViewController.parentController = self
        navigationController.pushViewController(addEditRuleViewController, animated: true)
    }
    
    func ledValueDidChange(relay: RelayModel) {
        RelayRouter(endpoint: .updateRelay(relay: relay)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
            guard let weakSelf = self else { return }
            if isSuccess {
                DispatchQueue.main.async {
                    weakSelf.ledTableView.reloadData()
                }
            }
        })
    }
}
