//
//  ItemMenuCell.swift
//  Smart-Agriculture
//
//  Created by admin on 05/11/2021.
//

import UIKit

class ItemMenuCell: BaseTBCell {
    @IBOutlet weak var lblItemTitle: UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setupData(data: String, type: Bool) {
        lblItemTitle.text = data
        lblItemTitle.textColor = .white
        if type {
            leadingConstraint.constant = 35
        } else {
            leadingConstraint.constant = 20
        }
    }
}
