//
//  SideMenuViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 05/11/2021.
//

import UIKit
import Kingfisher

protocol SideMenuViewControllerDelegate {
    func selectedCell(_ row: Int)
}

class SideMenuViewController: BaseViewController {
    @IBOutlet weak var sideMenuTableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    
    var delegate: SideMenuViewControllerDelegate?
    private var itemCellHeight: CGFloat = 50
    private var menuItem = [
        MenuItem.greenHouseSection.getTitle(),
        GreenHouseModel.shared.currentGreenHouse?.name ?? MenuItem.greenHouseItem.getTitle(),
        MenuItem.settings.getTitle(),
        MenuItem.setSchedule.getTitle(),
        MenuItem.scenario.getTitle(),
        MenuItem.rules.getTitle(),
        MenuItem.myAccount.getTitle(),
        MenuItem.logout.getTitle()
    ]
    var currentUser = UserModel.shared.currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
        // Setup username and avatar
        if let username = currentUser?.userInfo?.name {
            lblUsername.text = username
        }
        if let avatar = currentUser?.userInfo?.avatar {
            profileImage.maskCircle()
            let url = URL(string: "http://222.252.19.88:5000/\(avatar)")
            profileImage.kf.indicatorType = .activity
            profileImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }

    }
    
    func setupTableView() {
        ItemMenuCell.registerCellByNib(sideMenuTableView)
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        sideMenuTableView.reloadData()
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = ItemMenuCell.loadCell(tableView) as? ItemMenuCell else { return UITableViewCell() }
        var type: Bool?
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 6 {
            type = false
        } else {
            type = true
        }
        cell.setupData(data: menuItem[indexPath.row], type: type ?? false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return itemCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedCell(indexPath.row)
    }
}
