//
//  SelectOutputScriptViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 12/01/2022.
//

import UIKit
import IBAnimatable

protocol SelectOutputScriptViewControllerDelegate: AnyObject {
    func finishSelectOutputScript(returnData: RuleModel)
}

class SelectOutputScriptViewController: BaseViewController {
    @IBOutlet weak var shadowView: AnimatableView!
    @IBOutlet weak var scriptTableView: UITableView!
    
    private var scriptList = [RuleModel]()
    private var cellHeight: CGFloat = 169.5
    var rule = RuleModel()
    weak var delegate: SelectOutputScriptViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getListRuleInGreenhouse()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        scriptTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRuleInGreenhouse()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        OutputScriptCell.registerCellByNib(scriptTableView)
        scriptTableView.delegate = self
        scriptTableView.dataSource = self
    }
    
    @IBAction func clickToFinish(_ sender: Any) {
        delegate?.finishSelectOutputScript(returnData: rule)
        guard let navigationController = self.navigationController else { return }
        navigationController.popViewController(animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension SelectOutputScriptViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scriptList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = OutputScriptCell.loadCell(tableView) as? OutputScriptCell else { return UITableViewCell() }
        var isExist = false
        for execute in rule.execute {
            if let scriptInExecute = execute.id as? RuleModel {
                if scriptInExecute.id == scriptList[indexPath.row].id {
                    isExist = true
                    break
                }
            } else {
                isExist = false
            }
        }
        cell.delegate = self
        cell.setupData(script: scriptList[indexPath.row], isExist: isExist)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

// MARK: API CALL
extension SelectOutputScriptViewController {
    func getListRuleInGreenhouse() {
        guard let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId else { return }
        GreenHouseRouter(endpoint: .getListScenario(type: RuleType.script.rawValue, greenHouseId: currentGreenHouseId)).mapTo([RuleModel].self, success: { [weak self] (listScenario) in
            guard let weakSelf = self else { return }
            weakSelf.scriptList = listScenario
            weakSelf.scriptTableView.reloadData()
        })
    }
}

extension SelectOutputScriptViewController: OutputScriptCellDelegate {
    func isScriptSelect(script: Execute, isSelect: Bool) {
        if isSelect {
            rule.execute.append(script)
        } else {
            var deleteIndex = -1
            for i in 0 ..< rule.execute.count {
                guard let execute = rule.execute[i].id as? RuleModel else { return }
                guard let usingScript = script.id as? RuleModel else { return }
                if execute.id == usingScript.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex == -1 {
                rule.execute.remove(at: deleteIndex)
            }
        }
    }
}
