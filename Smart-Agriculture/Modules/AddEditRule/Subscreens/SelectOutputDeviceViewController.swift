//
//  SelectOutputDeviceViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 05/01/2022.
//

import UIKit
import IBAnimatable
import PagingKit

protocol SelectOutputDeviceViewControllerDelegate: AnyObject {
    func finishSelectOutputDevice(returnData: RuleModel)
}

class SelectOutputDeviceViewController: BaseViewController {
    @IBOutlet weak var shadowView: AnimatableView!
    
    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    let menuTitle = [GreenHouseMenuTitle.relay.rawValue, GreenHouseMenuTitle.led.rawValue]
    var selectedPage = 0
    var rule = RuleModel()
    weak var delegate: SelectOutputDeviceViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        menuViewController.register(nib: UINib(nibName: "GreenHouseMenuCell", bundle: nil), forCellWithReuseIdentifier: "GreenHouseMenuCell")
        menuViewController.registerFocusView(nib: UINib(nibName: "FocusView", bundle: nil))
        menuViewController.reloadData()
        contentViewController.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PagingContentViewController {
            contentViewController = viewController
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
        } else if let viewController = segue.destination as? PagingMenuViewController {
            menuViewController = viewController
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
        }
    }
    
    @IBAction func clickToFinish(_ sender: Any) {
        delegate?.finishSelectOutputDevice(returnData: rule)
        guard let navigationController = self.navigationController else { return }
        navigationController.popViewController(animated: true)
    }
}

// MARK: PagingContentViewControllerDataSource
extension SelectOutputDeviceViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return menuTitle.count
    }

    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        guard let outputDeviceViewController = R.storyboard.outputDevice.outputDeviceViewController() else { return UIViewController() }
        outputDeviceViewController.parentController = self
        outputDeviceViewController.deviceType = index
        outputDeviceViewController.rule = rule
        return outputDeviceViewController
    }
}

// MARK: PagingContentViewControllerDelegate
extension SelectOutputDeviceViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController?.scroll(index: index, percent: percent, animated: false)
        if percent == 0 {
            if selectedPage != index {
                selectedPage = index
            }
        }
    }
}

// MARK: PagingMenuViewControllerDataSource
extension SelectOutputDeviceViewController: PagingMenuViewControllerDataSource {
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return menuTitle.count
    }

    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return UIScreen.main.bounds.size.width / CGFloat(menuTitle.count)
    }

    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "GreenHouseMenuCell", for: index)
        if let greenHouseMenucell = cell as? GreenHouseMenuCell {
            greenHouseMenucell.setupData(data: menuTitle[index])
            greenHouseMenucell.focus(width: UIScreen.main.bounds.size.width / CGFloat(menuTitle.count))
        }
        return cell
    }
}

// MARK: PagingMenuViewControllerDelegate
extension SelectOutputDeviceViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        if selectedPage != page {
            selectedPage = page
            contentViewController?.scroll(to: page, animated: true)
        }
    }
}
