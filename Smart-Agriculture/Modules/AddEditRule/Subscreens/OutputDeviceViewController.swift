//
//  OutputDeviceViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 05/01/2022.
//

import UIKit

class OutputDeviceViewController: BaseViewController {
    @IBOutlet weak var relayTableView: UITableView!
    
    var deviceList = [RelayModel]()
    var currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    var deviceType = DeviceType.relay.rawValue
    var rule = RuleModel()
    private var relayCellHeight: CGFloat = 175
    private var ledCellHeight: CGFloat = 215
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getListRelay()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        relayTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRelay()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        OutputRelayCell.registerCellByNib(relayTableView)
        OutputLedCell.registerCellByNib(relayTableView)
        relayTableView.delegate = self
        relayTableView.dataSource = self
    }
}

extension OutputDeviceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if deviceType == DeviceType.relay.rawValue {
            guard let cell = OutputRelayCell.loadCell(tableView) as? OutputRelayCell else { return UITableViewCell() }
            var isExist = false
            var executeInfo = Execute()
            for execute in rule.execute {
                if let relayInExecute = execute.id as? RelayModel {
                    if relayInExecute.id == deviceList[indexPath.row].id {
                        executeInfo = execute
                        isExist = true
                        break
                    }
                } else {
                    isExist = false
                }
            }
            cell.delegate = self
            cell.setupData(relay: deviceList[indexPath.row], isExist: isExist, execute: executeInfo)
            return cell
        } else {
            guard let cell = OutputLedCell.loadCell(tableView) as? OutputLedCell else { return UITableViewCell() }
            var isExist = false
            var executeInfo = Execute()
            for execute in rule.execute {
                if let relayInExecute = execute.id as? RelayModel {
                    if relayInExecute.id == deviceList[indexPath.row].id {
                        executeInfo = execute
                        isExist = true
                        break
                    }
                } else {
                    isExist = false
                }
            }
            cell.delegate = self
            cell.setupData(relay: deviceList[indexPath.row], isExist: isExist, execute: executeInfo)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if deviceType == DeviceType.relay.rawValue {
            return relayCellHeight
        }
        return ledCellHeight
    }
}


// MARK: API CALL
extension OutputDeviceViewController {
    func getListRelay() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListRelayAndLed(greenHouseId: greenHouseId, type: deviceType)).mapTo([RelayModel].self, success: { [weak self] (listRelay) in
                guard let weakSelf = self else { return }
                weakSelf.deviceList = listRelay
                DispatchQueue.main.async {
                    weakSelf.relayTableView.reloadData()
                }
            })
        }
    }
}

// MARK: CELL DELEGATE
extension OutputDeviceViewController: OutputRelayCellDelegate {
    func isRelaySelected(device: Execute, isSelect: Bool) {
        if isSelect {
            rule.execute.append(device)
        } else {
            var deleteIndex = -1
            for i in 0 ..< rule.execute.count {
                guard let execute = rule.execute[i].id as? RelayModel else { return }
                guard let usingDevice = device.id as? RelayModel else { return }
                if execute.id == usingDevice.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex != -1 {
                rule.execute.remove(at: deleteIndex)
            }
        }
        guard let parentViewController = parentController as? SelectOutputDeviceViewController else { return }
        parentViewController.rule = rule
    }
    
    func delayOrDuringDidChange(device: Execute) {
        for i in 0 ..< rule.execute.count {
            guard let execute = rule.execute[i].id as? RelayModel else { return }
            guard let usingDevice = device.id as? RelayModel else { return }
            if execute.id == usingDevice.id {
                rule.execute[i] = device
            }
        }
    }
}

extension OutputDeviceViewController: OutputLedCellDelegate {
    func isLedSelected(device: Execute, isSelect: Bool) {
        if isSelect {
            rule.execute.append(device)
        } else {
            var deleteIndex = -1
            for i in 0 ..< rule.execute.count {
                guard let execute = rule.execute[i].id as? RelayModel else { return }
                guard let usingDevice = device.id as? RelayModel else { return }
                if execute.id == usingDevice.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex != -1 {
                rule.execute.remove(at: deleteIndex)
            }
        }
        guard let parentViewController = parentController as? SelectOutputDeviceViewController else { return }
        parentViewController.rule = rule
    }
    
    func ledValueDidChange(device: Execute) {
        for i in 0 ..< rule.execute.count {
            guard let execute = rule.execute[i].id as? RelayModel else { return }
            guard let usingDevice = device.id as? RelayModel else { return }
            if execute.id == usingDevice.id {
                rule.execute[i] = device
            }
        }
    }
}
