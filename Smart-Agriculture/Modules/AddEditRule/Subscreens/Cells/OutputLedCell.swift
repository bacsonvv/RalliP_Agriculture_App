//
//  OutputLedCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 11/01/2022.
//

import UIKit
import Kingfisher

protocol OutputLedCellDelegate: AnyObject {
    func isLedSelected(device: Execute, isSelect: Bool)
    func ledValueDidChange(device: Execute)
}

class OutputLedCell: BaseTBCell {
    @IBOutlet weak var lblLedName: UILabel!
    @IBOutlet weak var ledImage: UIImageView!
    @IBOutlet weak var btnLedSelect: UIButton!
    @IBOutlet weak var tfDuring: UITextField!
    @IBOutlet weak var tfDelay: UITextField!
    @IBOutlet weak var tfDim: UITextField!
    @IBOutlet weak var tfCct: UITextField!
    @IBOutlet weak var containerView: UIView!
    
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    weak var delegate: OutputLedCellDelegate?
    var isSelectedDevice = false
    var execute = Execute()
    var newSelectedDevice = RelayModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedCellCorner(view: containerView)
        self.selectionStyle = .none
        
        tfDelay.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDuring.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDim.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfCct.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDelay.delegate = self
        tfDuring.delegate = self
        tfDim.delegate = self
        tfCct.delegate = self
    }
    
    func setupData(relay: RelayModel, isExist: Bool, execute: Execute) {
        self.execute = execute
        self.newSelectedDevice = relay
        
        if let ledName = relay.name {
            lblLedName.text = ledName
        }
        if let ledImg = relay.avatar {
            let url = URL(string: ledImg)
            ledImage.kf.indicatorType = .activity
            ledImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        for property in relay.value {
            if property.name == DevicePropertyName.dim.rawValue {
                if let value = property.value {
                    tfDim.text = "\(value)"
                }
            }
            if property.name == DevicePropertyName.cct.rawValue {
                if let value = property.value {
                    tfCct.text = "\(value)"
                }
            }
        }
        if isExist {
            isSelectedDevice = true
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            tfDim.isUserInteractionEnabled = true
            tfCct.isUserInteractionEnabled = true
            btnLedSelect.setImage(checkImage, for: .normal)
        } else {
            isSelectedDevice = false
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            tfDim.isUserInteractionEnabled = false
            tfCct.isUserInteractionEnabled = false
            btnLedSelect.setImage(uncheckImage, for: .normal)
        }
        if let delay = execute.delay {
            tfDelay.text = "\(delay)"
        }
        if let during = execute.during {
            tfDuring.text = "\(during)"
        }
    }
    
    @IBAction func clickToSelectLed(_ sender: Any) {
        isSelectedDevice.toggle()
        if isSelectedDevice {
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            tfDim.isUserInteractionEnabled = true
            tfCct.isUserInteractionEnabled = true
            btnLedSelect.setImage(checkImage, for: .normal)
            if execute.id == nil {
                execute.id = newSelectedDevice
                execute.type = ExecuteType.relay.rawValue
                execute.during = Int(tfDelay.text ?? "") ?? 1
                execute.delay = Int(tfDuring.text ?? "") ?? 0
                execute.dim = Int(tfDim.text ?? "") ?? 1
                execute.cct = Int(tfCct.text ?? "") ?? 1
            }
            delegate?.isLedSelected(device: execute, isSelect: true)
        } else {
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            tfDim.isUserInteractionEnabled = false
            tfCct.isUserInteractionEnabled = false
            btnLedSelect.setImage(uncheckImage, for: .normal)
            delegate?.isLedSelected(device: execute, isSelect: false)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isNotEmpty ?? false {
            if textField == tfDelay {
                execute.delay = Int(tfDelay.text ?? "") ?? 0
                delegate?.ledValueDidChange(device: execute)
            } else if textField == tfDuring {
                execute.during = Int(tfDuring.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: execute)
            } else if textField == tfDim {
                execute.dim = Int(tfDim.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: execute)
            } else if textField == tfCct {
                execute.cct = Int(tfCct.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: execute)
            }
        }
    }
}

extension OutputLedCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
