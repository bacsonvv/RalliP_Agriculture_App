//
//  InputSensorCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 05/01/2022.
//

import UIKit
import Kingfisher

protocol InputSensorCellDelegate: AnyObject {
    func isSensorSelected(compare: CompareInCondition, sensor: SensorModel, isSelect: Bool)
    func compareEqualTypeDidSelect(compare: CompareInCondition)
    func sensorValueDidChange(compare: CompareInCondition)
}

class InputSensorCell: BaseTBCell {
    @IBOutlet weak var lblSensorName: UILabel!
    @IBOutlet weak var sensorImg: UIImageView!
    @IBOutlet weak var sensorType: UILabel!
    @IBOutlet weak var btnGreater: UIButton!
    @IBOutlet weak var btnLess: UIButton!
    @IBOutlet weak var sensorValue: UITextField!
    @IBOutlet weak var lblSensorUnit: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnSelectSensor: UIButton!
    
    private var checkImage = UIImage(named: "ic-check-radio")?.withRenderingMode(.alwaysOriginal)
    private var uncheckImage = UIImage(named: "ic-uncheck-radio")?.withRenderingMode(.alwaysOriginal)
    private let selectImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let unselectImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    private var isSensorSelect = false
    private var isGreaterSelect = true
    
    var returnCompare = CompareInCondition()
    var returnSensor = SensorModel()
    weak var delegate: InputSensorCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedCellCorner(view: containerView)
        self.selectionStyle = .none
        
        sensorValue.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        sensorValue.delegate = self
    }
    
    func setupData(compare: CompareInCondition?, sensor: SensorModel) {
        returnCompare = compare ?? CompareInCondition()
        returnSensor = sensor
        
        if let sensorName = sensor.name {
            lblSensorName.text = sensorName
        }
        
        switch sensor.type {
        case SensorType.temperature.rawValue:
            sensorType.text = SensorTypeName.temperature.rawValue
            lblSensorUnit.text = SensorUnitType.temperature.rawValue
        case SensorType.light.rawValue:
            sensorType.text = SensorTypeName.light.rawValue
            lblSensorUnit.text = SensorUnitType.light.rawValue
        case SensorType.humidity.rawValue:
            sensorType.text = SensorTypeName.humidity.rawValue
            lblSensorUnit.text = SensorUnitType.soilAndHumidity.rawValue
        case SensorType.soil.rawValue:
            sensorType.text = SensorTypeName.soil.rawValue
            lblSensorUnit.text = SensorUnitType.soilAndHumidity.rawValue
        default:
            sensorType.text = SensorTypeName.co2.rawValue
            lblSensorUnit.text = SensorUnitType.co2.rawValue
        }
        
        if let sensorImage = sensor.avatar {
            let url = URL(string: sensorImage)
            sensorImg.kf.indicatorType = .activity
            sensorImg.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        
        if let dataCompare = compare {
            if let value = dataCompare.value {
                sensorValue.text = "\(value)"
            }
            
            if let equal = dataCompare.equal {
                if equal == CompareEqualType.greater.rawValue {
                    isGreaterSelect = true
                    btnGreater.setImage(checkImage, for: .normal)
                    btnLess.setImage(uncheckImage, for: .normal)
                } else {
                    isGreaterSelect = false
                    btnGreater.setImage(uncheckImage, for: .normal)
                    btnLess.setImage(checkImage, for: .normal)
                }
            }
            
            btnSelectSensor.setImage(selectImage, for: .normal)
            isSensorSelect = true
            btnGreater.isUserInteractionEnabled = true
            btnLess.isUserInteractionEnabled = true
            sensorValue.isUserInteractionEnabled = true
        } else {
            sensorValue.text = "1"
            btnGreater.setImage(checkImage, for: .normal)
            btnLess.setImage(uncheckImage, for: .normal)
            btnSelectSensor.setImage(unselectImage, for: .normal)
            isSensorSelect = false
            btnGreater.isUserInteractionEnabled = false
            btnLess.isUserInteractionEnabled = false
            sensorValue.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func clickToSelectSensor(_ sender: Any) {
        isSensorSelect.toggle()
        if isSensorSelect {
            btnGreater.isUserInteractionEnabled = true
            btnLess.isUserInteractionEnabled = true
            sensorValue.isUserInteractionEnabled = true
            btnSelectSensor.setImage(selectImage, for: .normal)
            if returnCompare.id.id == nil {
                returnCompare.id = returnSensor
                if isGreaterSelect {
                    returnCompare.equal = CompareEqualType.greater.rawValue
                } else {
                    returnCompare.equal = CompareEqualType.lessThan.rawValue
                }
                returnCompare.value = Double(sensorValue.text ?? "") ?? 1.0
            }
            delegate?.isSensorSelected(compare: returnCompare, sensor: returnSensor, isSelect: true)
        } else {
            btnGreater.isUserInteractionEnabled = false
            btnLess.isUserInteractionEnabled = false
            sensorValue.isUserInteractionEnabled = false
            btnSelectSensor.setImage(unselectImage, for: .normal)
            delegate?.isSensorSelected(compare: returnCompare, sensor: returnSensor, isSelect: false)
        }
    }
    
    @IBAction func clickToSelectGreater(_ sender: Any) {
        if !isGreaterSelect {
            isGreaterSelect = true
            btnGreater.setImage(checkImage, for: .normal)
            btnLess.setImage(uncheckImage, for: .normal)
            returnCompare.equal = CompareEqualType.greater.rawValue
            delegate?.compareEqualTypeDidSelect(compare: returnCompare)
        }
    }
    
    @IBAction func clickToSelectLess(_ sender: Any) {
        if isGreaterSelect {
            isGreaterSelect = false
            btnGreater.setImage(uncheckImage, for: .normal)
            btnLess.setImage(checkImage, for: .normal)
            returnCompare.equal = CompareEqualType.lessThan.rawValue
            returnCompare.equal = CompareEqualType.lessThan.rawValue
            delegate?.compareEqualTypeDidSelect(compare: returnCompare)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isNotEmpty ?? false {
            returnCompare.value = Double(sensorValue.text ?? "") ?? 1.0
            delegate?.sensorValueDidChange(compare: returnCompare)
        }
    }
}

extension InputSensorCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
