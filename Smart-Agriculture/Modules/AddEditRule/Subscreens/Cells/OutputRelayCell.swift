//
//  OutputRelayCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 05/01/2022.
//

import UIKit
import Kingfisher

protocol OutputRelayCellDelegate: AnyObject {
    func isRelaySelected(device: Execute, isSelect: Bool)
    func delayOrDuringDidChange(device: Execute)
}

class OutputRelayCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblRelayName: UILabel!
    @IBOutlet weak var relayImage: UIImageView!
    @IBOutlet weak var btnSelectRelay: UIButton!
    @IBOutlet weak var tfDelay: UITextField!
    @IBOutlet weak var tfDuring: UITextField!
    
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    weak var delegate: OutputRelayCellDelegate?
    var isSelectedDevice = false
    var execute = Execute()
    var newSelectedDevice = RelayModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedCellCorner(view: containerView)
        self.selectionStyle = .none
        
        tfDelay.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDuring.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDelay.delegate = self
        tfDuring.delegate = self
    }
    
    func setupData(relay: RelayModel, isExist: Bool, execute: Execute) {
        self.execute = execute
        self.newSelectedDevice = relay
        
        if let relayName = relay.name {
            lblRelayName.text = relayName
        }
        if let relayImg = relay.avatar {
            let url = URL(string: relayImg)
            relayImage.kf.indicatorType = .activity
            relayImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if isExist {
            isSelectedDevice = true
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            btnSelectRelay.setImage(checkImage, for: .normal)
        } else {
            isSelectedDevice = false
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            btnSelectRelay.setImage(uncheckImage, for: .normal)
        }
        if let delay = execute.delay {
            tfDelay.text = "\(delay)"
        }
        if let during = execute.during {
            tfDuring.text = "\(during)"
        }
    }
    
    @IBAction func clickToSelectDevice(_ sender: Any) {
        isSelectedDevice.toggle()
        if isSelectedDevice {
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            btnSelectRelay.setImage(checkImage, for: .normal)
            if execute.id == nil {
                execute.id = newSelectedDevice
                execute.type = ExecuteType.relay.rawValue
                execute.during = Int(tfDelay.text ?? "") ?? 1
                execute.delay = Int(tfDuring.text ?? "") ?? 0
            }
            delegate?.isRelaySelected(device: execute, isSelect: true)
        } else {
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            btnSelectRelay.setImage(uncheckImage, for: .normal)
            delegate?.isRelaySelected(device: execute, isSelect: false)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isNotEmpty ?? false {
            if textField == tfDelay {
                execute.delay = Int(tfDelay.text ?? "") ?? 0
                delegate?.delayOrDuringDidChange(device: execute)
            } else {
                execute.during = Int(tfDuring.text ?? "") ?? 1
                delegate?.delayOrDuringDidChange(device: execute)
            }
        }
    }
}

extension OutputRelayCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
