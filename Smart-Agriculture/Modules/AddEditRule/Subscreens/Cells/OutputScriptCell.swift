//
//  OutputScriptCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 12/01/2022.
//

import UIKit

protocol OutputScriptCellDelegate: AnyObject {
    func isScriptSelect(script: Execute, isSelect: Bool)
}

class OutputScriptCell: BaseTBCell {
    @IBOutlet weak var btnSelectScript: UIButton!
    @IBOutlet weak var lblScriptName: UILabel!
    @IBOutlet weak var lblTotalDevice: UILabel!
    @IBOutlet weak var lblRelayNumber: UILabel!
    @IBOutlet weak var lblLedNumber: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    weak var delegate: OutputScriptCellDelegate?
    var isSelectedScript = false
    var execute = Execute()
    var newScriptSelected = RuleModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }
    
    func setupData(script: RuleModel, isExist: Bool) {
        newScriptSelected = script
        
        if let scriptName = script.name {
            lblScriptName.text = scriptName
        }
        if isExist {
            isSelectedScript = true
            btnSelectScript.setImage(checkImage, for: .normal)
        } else {
            isSelectedScript = false
            btnSelectScript.setImage(uncheckImage, for: .normal)
        }
        
        var numberOfRelay = 0
        var numberOfLed = 0
        for device in script.execute {
            guard let deviceInExcute = device.id as? RelayModel else { return }
            if deviceInExcute.type == DeviceType.relay.rawValue {
                numberOfRelay += 1
            } else {
                numberOfLed += 1
            }
        }
        
        lblRelayNumber.text = "\(numberOfRelay)"
        lblLedNumber.text = "\(numberOfLed)"
        
        let numberOfDevice = numberOfLed + numberOfRelay
        lblTotalDevice.text = "\(numberOfDevice)"
    }
    
    @IBAction func clickToSelectScript(_ sender: Any) {
        isSelectedScript.toggle()
        if isSelectedScript {
            btnSelectScript.setImage(checkImage, for: .normal)
            if execute.id == nil {
                execute.id = newScriptSelected
                execute.type = ExecuteType.script.rawValue
            }
            delegate?.isScriptSelect(script: execute, isSelect: true)
        } else {
            btnSelectScript.setImage(uncheckImage, for: .normal)
            delegate?.isScriptSelect(script: execute, isSelect: false)
        }
    }
}
