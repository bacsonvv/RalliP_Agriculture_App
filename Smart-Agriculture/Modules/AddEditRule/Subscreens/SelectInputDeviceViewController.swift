//
//  SelectInputDeviceViewController.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 05/01/2022.
//

import UIKit

protocol SelectInputDeviceViewControllerDelegate: AnyObject {
    func finishSelectInput(returnData: RuleModel)
}

class SelectInputDeviceViewController: BaseViewController {
    @IBOutlet weak var sensorTableView: UITableView!
    
    var listSensor = [SensorModel]()
    private let cellHeight: CGFloat = 220
    private let currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    var data = RuleModel()
    weak var delegate: SelectInputDeviceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Call api to get list of sensor in green house
        getListSensor()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        sensorTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListSensor()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        InputSensorCell.registerCellByNib(sensorTableView)
        sensorTableView.delegate = self
        sensorTableView.dataSource = self
    }
}

extension SelectInputDeviceViewController {
    @IBAction func clickToFinish(_ sender: Any) {
        delegate?.finishSelectInput(returnData: data)
        guard let navigationController = self.navigationController else { return }
        navigationController.popViewController(animated: true)
    }
}

extension SelectInputDeviceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSensor.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = InputSensorCell.loadCell(tableView) as? InputSensorCell else { return UITableViewCell() }
        let index = isSensorInRule(sensor: listSensor[indexPath.row], rule: data)
        if index != -1 {
            cell.setupData(compare: data.input.condition.compare[index], sensor: data.input.condition.compare[index].id)
        } else {
            cell.setupData(compare: nil, sensor: listSensor[indexPath.row])
        }
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

// MARK: API Call
extension SelectInputDeviceViewController {
    func getListSensor() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListSensor(greenHouseId: greenHouseId)).mapTo([SensorModel].self, success: { [weak self] (listSensor) in
                guard let weakSelf = self else { return }
                weakSelf.listSensor = listSensor
                weakSelf.sensorTableView.reloadData()
            })
        }
    }
}

extension SelectInputDeviceViewController {
    func isSensorInRule(sensor: SensorModel, rule: RuleModel) -> Int {
        guard let sensorId = sensor.id else { return -1 }
        let compares = rule.input.condition.compare
        
        if compares.isNotEmpty {
            for i in 0 ..< compares.count {
                if let compareId = compares[i].id.id {
                    if compareId == sensorId {
                        return i
                    }
                }
            }
        }
        
        return -1
    }
}

extension SelectInputDeviceViewController: InputSensorCellDelegate {
    func isSensorSelected(compare: CompareInCondition, sensor: SensorModel, isSelect: Bool) {
        if isSelect {
            data.input.condition.compare.append(compare)
        } else {
            var deleteIndex = -1
            for i in 0 ..< data.input.condition.compare.count {
                if data.input.condition.compare[i].id.id == compare.id.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex != -1 {
                data.input.condition.compare.remove(at: deleteIndex)
            }
        }
    }
    
    func compareEqualTypeDidSelect(compare: CompareInCondition) {
        for i in 0 ..< data.input.condition.compare.count {
            if data.input.condition.compare[i].id.id == compare.id.id {
                data.input.condition.compare[i].equal = compare.equal
            }
        }
    }
    
    func sensorValueDidChange(compare: CompareInCondition) {
        for i in 0 ..< data.input.condition.compare.count {
            if data.input.condition.compare[i].id.id == compare.id.id {
                data.input.condition.compare[i].value = compare.value
            }
        }
    }
    
    
}
