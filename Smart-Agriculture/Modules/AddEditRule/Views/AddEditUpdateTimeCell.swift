//
//  AddEditUpdateTimeCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 24/12/2021.
//

import UIKit

class AddEditUpdateTimeCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblRuleTime: UILabel!
    @IBOutlet weak var lblRuleTimeValue: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        lblRuleTime.textColor = UIColor.colorFromHexString(hex: "#13313D")
        lblRuleTimeValue.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lblRuleTimeValue.delegate = self
        self.selectionStyle = .none
    }
    
    func setupData(rule: RuleModel, screenType: String) {
        if screenType == ScreenType.edit.rawValue {
            if let ruleTime = rule.input.schedule.timeUpdate {
                lblRuleTimeValue.text = "\(ruleTime)"
            }
        } else {
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            lblRuleTimeValue.text = "1"
            parentViewController.data.input.schedule.timeUpdate = 1
        }
    }
}

extension AddEditUpdateTimeCell {
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if textField.text?.isNotEmpty ?? false {
            parentViewController.data.input.schedule.timeUpdate = Int(textField.text ?? "") ?? 0
        }
    }
}

extension AddEditUpdateTimeCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
