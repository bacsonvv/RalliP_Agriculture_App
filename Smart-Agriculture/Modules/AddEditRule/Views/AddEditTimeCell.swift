//
//  AddEditTimeCell.swift
//  Smart-Agriculture
//
//  Created by admin on 16/11/2021.
//

import UIKit

class AddEditTimeCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblRuleTime: UILabel!
    @IBOutlet weak var lblRuleTimeValue: UITextField!
    
    let timePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var date = Date()
    let calendar = Calendar.current
    var timeType = TimeType.start.rawValue
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        lblRuleTime.textColor = UIColor.colorFromHexString(hex: "#13313D")
        self.selectionStyle = .none
        lblRuleTimeValue.delegate = self
        
        createTimePicker()
    }
    
    func setupData(rule: RuleModel, screenType: String) {
        if timeType == TimeType.start.rawValue {
            lblRuleTime.text = "Thời gian bắt đầu"
        } else {
            lblRuleTime.text = "Thời gian kết thúc"
        }
        
        if screenType == ScreenType.edit.rawValue {
            if timeType == TimeType.start.rawValue {
                if let ruleTime = rule.input.schedule.timeStart {
                    lblRuleTimeValue.text = "\(ruleTime)"
                }
            } else {
                if let ruleTime = rule.input.schedule.timeStop {
                    lblRuleTimeValue.text = "\(ruleTime)"
                }
            }
        } else {
            dateFormatter.dateFormat = "HH:mm"
            lblRuleTimeValue.text = dateFormatter.string(from: date)
            
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            let serverPutTimeString = convertTo12FormatFromDate()
            if timeType == TimeType.start.rawValue {
                parentViewController.data.input.schedule.timeStart = serverPutTimeString
            } else {
                parentViewController.data.input.schedule.timeStop = serverPutTimeString
            }
        }
    }
    
    func createTimePicker() {
        timePicker.datePickerMode = .time
        if #available(iOS 13.4, *) {
            timePicker.preferredDatePickerStyle = .wheels
        }
        
        // Toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // Bar button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(timePickingDone(_:)))
        toolbar.setItems([doneButton], animated: true)
        
        // Assign toolbar
        lblRuleTimeValue.inputAccessoryView = toolbar
        
        // Assign time picker to the text field
        lblRuleTimeValue.inputView = timePicker
    }
    
    @objc func timePickingDone(_ barButtonItem: UIBarButtonItem) {
        date = timePicker.date
        
        let serverPutTimeString = convertTo12FormatFromDate()
        
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if timeType == TimeType.start.rawValue {
            parentViewController.data.input.schedule.timeStart = serverPutTimeString
        } else {
            parentViewController.data.input.schedule.timeStop = serverPutTimeString
        }
        
        lblRuleTimeValue.text = serverPutTimeString

        self.contentView.endEditing(true)
    }
}

extension AddEditTimeCell {
    func convertTo12FormatFromDate() -> String {
        var serverPutTimeString = ""
        var hourString = ""
        var minuteString = ""
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        if hour < 10 {
            hourString = "0\(hour)"
        } else {
            hourString = "\(hour)"
        }
        
        if minutes < 10 {
            minuteString = "0\(minutes)"
        } else {
            minuteString = "\(minutes)"
        }
        
        serverPutTimeString = "\(hourString):\(minuteString)"
        
        return serverPutTimeString
    }
}

extension AddEditTimeCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}
