//
//  AddEditStatusCell.swift
//  Smart-Agriculture
//
//  Created by admin on 15/11/2021.
//

import UIKit

class AddEditStatusCell: BaseTBCell {
    @IBOutlet weak var lblScheduleStatus: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        lblScheduleStatus.textColor = UIColor.colorFromHexString(hex: "#13313D")
        btnSwitch.scale(x: 0.75, y: 0.75)
        self.selectionStyle = .none
    }
    
    func setupData(state: Int, screenType: String) {
        if screenType == ScreenType.edit.rawValue {
            if state == 1 {
                lblStatus.text = SwitchState.on.rawValue
                btnSwitch.setOn(true, animated: false)
            } else {
                lblStatus.text = SwitchState.off.rawValue
                btnSwitch.setOn(false, animated: false)
            }
        } else {
            lblStatus.text = SwitchState.off.rawValue
            btnSwitch.setOn(false, animated: false)
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            parentViewController.data.execute[0].pull = state
        }
    }
}

extension AddEditStatusCell {
    @IBAction func clickToChangeStatus(_ sender: Any) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if btnSwitch.isOn {
            lblStatus.text = SwitchState.on.rawValue
            if parentViewController.data.execute.isNotEmpty {
                parentViewController.data.execute[0].pull = 1
            }
        } else {
            lblStatus.text = SwitchState.off.rawValue
            if parentViewController.data.execute.isNotEmpty {
                parentViewController.data.execute[0].pull = 0
            }
        }
    }
}
