//
//  SelectingDeviceCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 22/12/2021.
//

import UIKit
import Kingfisher

protocol SelectingDeviceCellDelegate: AnyObject {
    func isDeviceSelected(device: Execute, isSelect: Bool)
    func delayOrDuringDidChange(device: Execute)
}

class SelectingDeviceCell: BaseTBCell {
    @IBOutlet weak var lblDeviceName: UILabel!
    @IBOutlet weak var selectedDeviceImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnIsCellCheck: UIButton!
    @IBOutlet weak var tfDelay: UITextField!
    @IBOutlet weak var tfDuring: UITextField!
    
    var isSelectedDevice = false
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    weak var delegate: SelectingDeviceCellDelegate?
    var data = Execute()
    var newSelectedDevice = RelayModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
        
        tfDelay.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDuring.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDelay.delegate = self
        tfDuring.delegate = self
    }

    func setupData(data: RelayModel, isExist: Bool, execute: Execute) {
        self.data = execute
        self.newSelectedDevice = data
        
        if let name = data.name {
            lblDeviceName.text = name
        }
        if let image = data.avatar {
            let url = URL(string: image)
            selectedDeviceImage.kf.indicatorType = .activity
            selectedDeviceImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if isExist {
            isSelectedDevice = true
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            btnIsCellCheck.setImage(checkImage, for: .normal)
        } else {
            isSelectedDevice = false
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            btnIsCellCheck.setImage(uncheckImage, for: .normal)
        }
        if let delay = execute.delay {
            tfDelay.text = "\(delay)"
        }
        if let during = execute.during {
            tfDuring.text = "\(during)"
        }
    }
    
    @IBAction func clickToSelectDevice(_ sender: Any) {
        isSelectedDevice.toggle()
        if isSelectedDevice {
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            btnIsCellCheck.setImage(checkImage, for: .normal)
            if data.id == nil {
                data.id = newSelectedDevice
                data.type = ExecuteType.relay.rawValue
                data.during = Int(tfDelay.text ?? "") ?? 1
                data.delay = Int(tfDuring.text ?? "") ?? 0
            }
            delegate?.isDeviceSelected(device: data, isSelect: true)
        } else {
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            btnIsCellCheck.setImage(uncheckImage, for: .normal)
            delegate?.isDeviceSelected(device: data, isSelect: false)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isNotEmpty ?? false {
            if textField == tfDelay {
                data.delay = Int(tfDelay.text ?? "") ?? 0
                delegate?.delayOrDuringDidChange(device: data)
            } else {
                data.during = Int(tfDuring.text ?? "") ?? 1
                delegate?.delayOrDuringDidChange(device: data)
            }
        }
    }
}

extension SelectingDeviceCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
