//
//  AddEditRepeatCell.swift
//  Smart-Agriculture
//
//  Created by admin on 16/11/2021.
//

import UIKit

class AddEditRepeatCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblScheduleRepeat: UILabel!
    @IBOutlet weak var btnMondayCheck: UIButton!
    @IBOutlet weak var btnTuesdayCheck: UIButton!
    @IBOutlet weak var btnWednesdayCheck: UIButton!
    @IBOutlet weak var btnThursdayCheck: UIButton!
    @IBOutlet weak var btnFridayCheck: UIButton!
    @IBOutlet weak var btnSaturdayCheck: UIButton!
    @IBOutlet weak var btnSundayCheck: UIButton!
    
    private var data: DayLoop = DayLoop()
    
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
        setupDefaultDayLoop(buttons: [btnMondayCheck, btnTuesdayCheck, btnWednesdayCheck, btnThursdayCheck, btnFridayCheck, btnSaturdayCheck, btnSundayCheck])
    }
    
    func setupDefaultDayLoop(buttons: [UIButton]) {
        buttons.forEach { button in
            button.setImage(uncheckImage, for: .normal)
        }
    }
    
    func setupData(schedule: RuleModel) {
        data = schedule.input.loop
        let loop = schedule.input.loop
        if loop.monday {
            data.monday = true
            btnMondayCheck.setImage(checkImage, for: .normal)
        } else {
            data.monday = false
            btnMondayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.tuesday {
            data.tuesday = true
            btnTuesdayCheck.setImage(checkImage, for: .normal)
        } else {
            data.tuesday = false
            btnTuesdayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.wednesday {
            data.wednesday = true
            btnWednesdayCheck.setImage(checkImage, for: .normal)
        } else {
            data.wednesday = false
            btnWednesdayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.thursday {
            data.thursday = true
            btnThursdayCheck.setImage(checkImage, for: .normal)
        } else {
            data.thursday = false
            btnThursdayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.friday {
            data.friday = true
            btnFridayCheck.setImage(checkImage, for: .normal)
        } else {
            data.friday = false
            btnFridayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.saturday {
            data.saturday = true
            btnSaturdayCheck.setImage(checkImage, for: .normal)
        } else {
            data.saturday = false
            btnSaturdayCheck.setImage(uncheckImage, for: .normal)
        }
        if loop.sunday {
            data.sunday = true
            btnSundayCheck.setImage(checkImage, for: .normal)
        } else {
            data.sunday = false
            btnSundayCheck.setImage(uncheckImage, for: .normal)
        }
    }
}

// MARK: Handle IBActions
extension AddEditRepeatCell {
    @IBAction func clickToMondayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t2.rawValue)
    }
    @IBAction func clickToTuesdayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t3.rawValue)
    }
    @IBAction func clickToWednesdayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t4.rawValue)
    }
    @IBAction func clickToThursdayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t5.rawValue)
    }
    @IBAction func clickToFridayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t6.rawValue)
    }
    @IBAction func clickToSaturdayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.t7.rawValue)
    }
    @IBAction func clickToSundayCheck(_ sender: Any) {
        handleButtonWithDay(day: RepeatText.cn.rawValue)
    }
}

extension AddEditRepeatCell {
    func handleButtonWithDay(day: String) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        switch day {
        case RepeatText.t2.rawValue:
            data.monday.toggle()
            if data.monday {
                parentViewController.data.input.loop.monday = true
                btnMondayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.monday = false
                btnMondayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.t3.rawValue:
            data.tuesday.toggle()
            if data.tuesday {
                parentViewController.data.input.loop.tuesday = true
                btnTuesdayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.tuesday = false
                btnTuesdayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.t4.rawValue:
            data.wednesday.toggle()
            if data.wednesday {
                parentViewController.data.input.loop.wednesday = true
                btnWednesdayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.wednesday = false
                btnWednesdayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.t5.rawValue:
            data.thursday.toggle()
            if data.thursday {
                parentViewController.data.input.loop.thursday = true
                btnThursdayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.thursday = false
                btnThursdayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.t6.rawValue:
            data.friday.toggle()
            if data.friday {
                parentViewController.data.input.loop.friday = true
                btnFridayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.friday = false
                btnFridayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.t7.rawValue:
            data.saturday.toggle()
            if data.saturday {
                parentViewController.data.input.loop.saturday = true
                btnSaturdayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.saturday = false
                btnSaturdayCheck.setImage(uncheckImage, for: .normal)
            }
        case RepeatText.cn.rawValue:
            data.sunday.toggle()
            if data.sunday {
                parentViewController.data.input.loop.sunday = true
                btnSundayCheck.setImage(checkImage, for: .normal)
            } else {
                parentViewController.data.input.loop.sunday = false
                btnSundayCheck.setImage(uncheckImage, for: .normal)
            }
        default:
            break
        }
    }
}
