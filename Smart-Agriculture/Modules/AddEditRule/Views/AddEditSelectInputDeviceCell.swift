//
//  AddEditSelectInputDeviceCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 24/12/2021.
//

import UIKit

class AddEditSelectInputDeviceCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
    }
}
