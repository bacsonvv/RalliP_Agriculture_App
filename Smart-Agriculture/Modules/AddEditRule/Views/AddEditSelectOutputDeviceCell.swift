//
//  AddEditSelectOutputDeviceCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 24/12/2021.
//

import UIKit

protocol AddEditSelectOutputDeviceCellDelegate: AnyObject {
    func outputSelectionTypeDidChange(type: String)
}

class AddEditSelectOutputDeviceCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnRelaySelect: UIButton!
    @IBOutlet weak var btnScriptSelect: UIButton!
    
    private var checkImage = UIImage(named: "ic-check-radio")?.withRenderingMode(.alwaysOriginal)
    private var uncheckImage = UIImage(named: "ic-uncheck-radio")?.withRenderingMode(.alwaysOriginal)
    private var isRelaySelect = false
    weak var delegate: AddEditSelectOutputDeviceCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
    }
    
    func setupData(data: RuleModel, screenType: String) {
        if screenType == ScreenType.edit.rawValue {
            if data.execute.isNotEmpty {
                let execute = data.execute[0]
                if execute.type == ExecuteType.relay.rawValue {
                    btnRelaySelect.setImage(checkImage, for: .normal)
                    btnScriptSelect.setImage(uncheckImage, for: .normal)
                    isRelaySelect = true
                } else {
                    btnRelaySelect.setImage(uncheckImage, for: .normal)
                    btnScriptSelect.setImage(checkImage, for: .normal)
                    isRelaySelect = false
                }
            } else {
                btnRelaySelect.setImage(checkImage, for: .normal)
                btnScriptSelect.setImage(uncheckImage, for: .normal)
                isRelaySelect = true
            }
        } else {
            btnRelaySelect.setImage(checkImage, for: .normal)
            btnScriptSelect.setImage(uncheckImage, for: .normal)
            isRelaySelect = true
        }
    }
    
    @IBAction func clickToChooseRelayOutput(_ sender: Any) {
        if !isRelaySelect {
            isRelaySelect = true
            btnRelaySelect.setImage(checkImage, for: .normal)
            btnScriptSelect.setImage(uncheckImage, for: .normal)
            delegate?.outputSelectionTypeDidChange(type: ExecuteType.relay.rawValue)
        }
    }
    @IBAction func clickToChooseScriptOutput(_ sender: Any) {
        if isRelaySelect {
            isRelaySelect = false
            btnRelaySelect.setImage(uncheckImage, for: .normal)
            btnScriptSelect.setImage(checkImage, for: .normal)
            delegate?.outputSelectionTypeDidChange(type: ExecuteType.script.rawValue)
        }
    }
}
