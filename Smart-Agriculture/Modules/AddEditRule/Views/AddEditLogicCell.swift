//
//  AddEditLogicCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 24/12/2021.
//

import UIKit

class AddEditLogicCell: BaseTBCell {
    @IBOutlet weak var btnAndRadio: UIButton!
    @IBOutlet weak var btnOrRadio: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    private var checkImage = UIImage(named: "ic-check-radio")?.withRenderingMode(.alwaysOriginal)
    private var uncheckImage = UIImage(named: "ic-uncheck-radio")?.withRenderingMode(.alwaysOriginal)
    private var isAndSelect = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
    }
    
    func setupData(rule: RuleModel) {
        if let logic = rule.input.condition.logic {
            if logic == LogicType.and.rawValue {
                isAndSelect = true
                btnAndRadio.setImage(checkImage, for: .normal)
                btnOrRadio.setImage(uncheckImage, for: .normal)
            } else {
                isAndSelect = false
                btnAndRadio.setImage(uncheckImage, for: .normal)
                btnOrRadio.setImage(checkImage, for: .normal)
            }
        } else {
            isAndSelect = true
            btnAndRadio.setImage(checkImage, for: .normal)
            btnOrRadio.setImage(uncheckImage, for: .normal)
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            parentViewController.data.input.condition.logic = LogicType.and.rawValue
        }
    }
    
    @IBAction func clickToSelectAndLogic(_ sender: Any) {
        if !isAndSelect {
            isAndSelect = true
            btnAndRadio.setImage(checkImage, for: .normal)
            btnOrRadio.setImage(uncheckImage, for: .normal)
            
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            parentViewController.data.input.condition.logic = LogicType.and.rawValue
        }
    }
    @IBAction func clickToSelectOrLogic(_ sender: Any) {
        if isAndSelect {
            isAndSelect = false
            btnAndRadio.setImage(uncheckImage, for: .normal)
            btnOrRadio.setImage(checkImage, for: .normal)
            
            guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
            parentViewController.data.input.condition.logic = LogicType.or.rawValue
        }
    }
}
