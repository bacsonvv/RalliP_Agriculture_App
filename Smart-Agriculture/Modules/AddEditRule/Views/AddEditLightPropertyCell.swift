//
//  AddEditLightPropertyCell.swift
//  Smart-Agriculture
//
//  Created by admin on 15/11/2021.
//

import UIKit

class AddEditLightPropertyCell: BaseTBCell {
    @IBOutlet weak var dimSlider: UISlider!
    @IBOutlet weak var dimValue: UITextField!
    @IBOutlet weak var cctSlider: UISlider!
    @IBOutlet weak var cctValue: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblScheduleStatus: UILabel!
    
    private var schedule = RuleModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
        
        dimValue.isUserInteractionEnabled = false
        cctValue.isUserInteractionEnabled = false

        self.roundedCellCorner(view: containerView)
        self.selectionStyle = .none
    }
    
    func setupSliderValue(sliders: [UISlider]) {
        sliders.forEach { slider in
            slider.minimumValue = 1
            slider.maximumValue = 100
        }
        dimSlider.setValue(1, animated: false)
        cctSlider.setValue(1, animated: false)
        dimValue.text = "1"
        cctValue.text = "1"
    }
    
    func setupView() {
        self.roundedCellCorner(view: containerView)
        setupSliderValue(sliders: [dimSlider, cctSlider])
        lblScheduleStatus.textColor = UIColor.colorFromHexString(hex: "#13313D")
    }
    
    func setupData(schedule: RuleModel) {
        self.schedule = schedule
        
        if schedule.execute.isNotEmpty {
            if let dim = schedule.execute[0].dim {
                dimValue.text = "\(dim)"
                dimSlider.setValue(Float(dim), animated: false)
            }
            if let cct = schedule.execute[0].cct {
                cctValue.text = "\(cct)"
                cctSlider.setValue(Float(cct), animated: false)
            }
        }
    }
}

// MARK: Handle slider action
extension AddEditLightPropertyCell {
    @IBAction func clickToChangeDim(_ sender: Any) {
        dimValue.text = String(Int(dimSlider.value))
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        parentViewController.data.execute[0].dim = Int(dimSlider.value)
    }
    @IBAction func clickToChangeCct(_ sender: Any) {
        cctValue.text = String(Int(cctSlider.value))
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        parentViewController.data.execute[0].cct = Int(cctSlider.value)
    }
}

