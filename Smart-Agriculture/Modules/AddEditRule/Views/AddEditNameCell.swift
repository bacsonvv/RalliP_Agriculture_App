//
//  AddEditNameCell.swift
//  Smart-Agriculture
//
//  Created by admin on 15/11/2021.
//

import UIKit

class AddEditNameCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tfRuleName: UITextField!
    @IBOutlet weak var lblCellName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedCellCorner(view: containerView)
        lblCellName.textColor = UIColor.colorFromHexString(hex: "#13313D")
        tfRuleName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.selectionStyle = .none
    }
    
    func setupData(scheduleName: String, ruleType: Int) {
        tfRuleName.text = scheduleName
        switch ruleType {
        case RuleType.script.rawValue:
            tfRuleName.placeholder = "Tên kịch bản"
            lblCellName.text = "Tên kịch bản thiết bị"
        case RuleType.rule.rawValue:
            lblCellName.text = "Tên rule thiết bị"
            tfRuleName.placeholder = "Tên rule"
        case RuleType.schedule.rawValue:
            lblCellName.text = "Tên lịch thiết bị"
            tfRuleName.placeholder = "Tên lịch"
        default:
            break
        }
    }
}

extension AddEditNameCell {
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if textField.text?.isNotEmpty ?? false {
            parentViewController.data.name = textField.text
        }
    }
}
