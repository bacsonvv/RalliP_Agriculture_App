//
//  AddEditDeviceCell.swift
//  Smart-Agriculture
//
//  Created by admin on 18/11/2021.
//

import UIKit

class AddEditDeviceCell: BaseTBCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedDeviceTableView: UITableView!
    @IBOutlet weak var relaySelectedView: UIView!
    @IBOutlet weak var ledSelectedView: UIView!
    
    var data = RuleModel()
    var relayList = [RelayModel]()
    var ledList = [RelayModel]()
    var deviceSelected = DeviceType.relay.rawValue
    var currentGreenHouseId = UserModel.shared.currentUser?.userInfo?.greenHouseId
    private let relayCellHeight: CGFloat = 200
    private let ledCellHeight: CGFloat = 228
    var screenType = ScreenType.add.rawValue
    let refreshControl = UIRefreshControl()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
        
        relaySelectedView.backgroundColor = .blue
        ledSelectedView.backgroundColor = .white
        
        getListRelay()
        setupPullToRefresh()
        setupTableView()
    }
    
    func setupPullToRefresh() {
        self.refreshControl.attributedTitle = NSAttributedString(string: ConstantText.refreshDataMessage)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        selectedDeviceTableView.refreshControl = self.refreshControl
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getListRelay()
        self.refreshControl.endRefreshing()
    }
    
    func setupTableView() {
        SelectingLedCell.registerCellByNib(selectedDeviceTableView)
        SelectingDeviceCell.registerCellByNib(selectedDeviceTableView)
        selectedDeviceTableView.delegate = self
        selectedDeviceTableView.dataSource = self
    }
}

extension AddEditDeviceCell {
    @IBAction func clickToSelectRelayList(_ sender: Any) {
        deviceSelected = DeviceType.relay.rawValue
        relaySelectedView.backgroundColor = .blue
        ledSelectedView.backgroundColor = .white
        getListRelay()
        
    }
    @IBAction func clickToSelectLedList(_ sender: Any) {
        deviceSelected = DeviceType.led.rawValue
        relaySelectedView.backgroundColor = .white
        ledSelectedView.backgroundColor = .blue
        getListRelay()
    }
}

extension AddEditDeviceCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if deviceSelected == DeviceType.relay.rawValue {
            return relayList.count
        }
        return ledList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if deviceSelected == DeviceType.relay.rawValue {
            guard let cell = SelectingDeviceCell.loadCell(tableView) as? SelectingDeviceCell else { return UITableViewCell() }
            var isExist = false
            var executeInfo = Execute()
            for execute in data.execute {
                guard let relayInExecute = execute.id as? RelayModel else { return UITableViewCell() }
                if relayInExecute.id == relayList[indexPath.row].id {
                    executeInfo = execute
                    isExist = true
                    break
                }
            }
            cell.delegate = self
            cell.setupData(data: relayList[indexPath.row], isExist: isExist, execute: executeInfo)
            return cell
        }  else {
            guard let cell = SelectingLedCell.loadCell(tableView) as? SelectingLedCell else { return UITableViewCell() }
            var isExist = false
            var executeInfo = Execute()
            for execute in data.execute {
                guard let relayInExecute = execute.id as? RelayModel else { return UITableViewCell() }
                if relayInExecute.id == ledList[indexPath.row].id {
                    executeInfo = execute
                    isExist = true
                    break
                }
            }
            cell.delegate = self
            cell.setupData(data: ledList[indexPath.row], isExist: isExist, execute: executeInfo)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if deviceSelected == DeviceType.relay.rawValue {
            return relayCellHeight
        }
        return ledCellHeight
    }
}

// MARK: API Call
extension AddEditDeviceCell {
    func getListRelay() {
        if let greenHouseId = currentGreenHouseId {
            GreenHouseRouter(endpoint: .getListRelayAndLed(greenHouseId: greenHouseId, type: deviceSelected)).mapTo([RelayModel].self, success: { [weak self] (listRelay) in
                guard let weakSelf = self else { return }
                weakSelf.relayList = listRelay.filter{ $0.type == DeviceType.relay.rawValue }
                weakSelf.ledList = listRelay.filter{ $0.type == DeviceType.led.rawValue }
                DispatchQueue.main.async {
                    weakSelf.selectedDeviceTableView.reloadData()
                }
            })
        }
    }
}

extension AddEditDeviceCell: SelectingDeviceCellDelegate {
    func isDeviceSelected(device: Execute, isSelect: Bool) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if isSelect {
            parentViewController.data.execute.append(device)
        } else {
            var deleteIndex = -1
            for i in 0 ..< parentViewController.data.execute.count {
                guard let execute = parentViewController.data.execute[i].id as? RelayModel else { return }
                guard let usingDevice = device.id as? RelayModel else { return }
                if execute.id == usingDevice.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex != -1 {
                parentViewController.data.execute.remove(at: deleteIndex)
            }
        }
    }
    
    func delayOrDuringDidChange(device: Execute) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        for i in 0 ..< parentViewController.data.execute.count {
            guard let execute = parentViewController.data.execute[i].id as? RelayModel else { return }
            guard let usingDevice = device.id as? RelayModel else { return }
            if execute.id == usingDevice.id {
                parentViewController.data.execute[i] = device
            }
        }
    }
}

extension AddEditDeviceCell: SelectingLedCellDelegate {
    func isLedSelected(device: Execute, isSelect: Bool) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        if isSelect {
            parentViewController.data.execute.append(device)
        } else {
            var deleteIndex = -1
            for i in 0 ..< parentViewController.data.execute.count {
                guard let execute = parentViewController.data.execute[i].id as? RelayModel else { return }
                guard let usingDevice = device.id as? RelayModel else { return }
                if execute.id == usingDevice.id {
                    deleteIndex = i
                    break
                }
            }
            if deleteIndex != -1 {
                parentViewController.data.execute.remove(at: deleteIndex)
            }
        }
    }
    
    func ledValueDidChange(device: Execute) {
        guard let parentViewController = parentViewController as? AddEditRuleViewController else { return }
        for i in 0 ..< parentViewController.data.execute.count {
            guard let execute = parentViewController.data.execute[i].id as? RelayModel else { return }
            guard let usingDevice = device.id as? RelayModel else { return }
            if execute.id == usingDevice.id {
                parentViewController.data.execute[i] = device
            }
        }
    }
}
