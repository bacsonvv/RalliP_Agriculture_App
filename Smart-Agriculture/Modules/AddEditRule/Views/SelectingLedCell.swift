//
//  SelectingLedCell.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 13/01/2022.
//

import UIKit
import Kingfisher

protocol SelectingLedCellDelegate: AnyObject {
    func isLedSelected(device: Execute, isSelect: Bool)
    func ledValueDidChange(device: Execute)
}

class SelectingLedCell: BaseTBCell {
    @IBOutlet weak var lblLedName: UILabel!
    @IBOutlet weak var btnSelectLed: UIButton!
    @IBOutlet weak var tfDimValue: UITextField!
    @IBOutlet weak var tfCctValue: UITextField!
    @IBOutlet weak var tfDelay: UITextField!
    @IBOutlet weak var tfDuring: UITextField!
    @IBOutlet weak var ledImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    var isSelectedLed = false
    private let checkImage = UIImage(named: "ic-checked-button")?.withRenderingMode(.alwaysOriginal)
    private let uncheckImage = UIImage(named: "ic-uncheck-button")?.withRenderingMode(.alwaysOriginal)
    weak var delegate: SelectingLedCellDelegate?
    var data = Execute()
    var newSelectedDevice = RelayModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.roundedCellCorner(view: containerView)
        
        tfDelay.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDuring.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDimValue.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfCctValue.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfDelay.delegate = self
        tfDuring.delegate = self
        tfDimValue.delegate = self
        tfCctValue.delegate = self
    }
    
    func setupData(data: RelayModel, isExist: Bool, execute: Execute) {
        self.data = execute
        self.newSelectedDevice = data
        
        if let name = data.name {
            lblLedName.text = name
        }
        if let image = data.avatar {
            let url = URL(string: image)
            ledImage.kf.indicatorType = .activity
            ledImage.kf.setImage(with: url, placeholder: UIImage(named: "ic-app-logo"))
        }
        if isExist {
            isSelectedLed = true
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            tfDimValue.isUserInteractionEnabled = true
            tfCctValue.isUserInteractionEnabled = true
            btnSelectLed.setImage(checkImage, for: .normal)
        } else {
            isSelectedLed = false
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            tfDimValue.isUserInteractionEnabled = false
            tfCctValue.isUserInteractionEnabled = false
            btnSelectLed.setImage(uncheckImage, for: .normal)
        }
        if let delay = execute.delay {
            tfDelay.text = "\(delay)"
        }
        if let during = execute.during {
            tfDuring.text = "\(during)"
        }
        if let dim = execute.dim {
            tfDimValue.text = "\(dim)"
        }
        if let cct = execute.cct {
            tfCctValue.text = "\(cct)"
        }
    }
    
    @IBAction func clickToSelectLed(_ sender: Any) {
        isSelectedLed.toggle()
        if isSelectedLed {
            tfDelay.isUserInteractionEnabled = true
            tfDuring.isUserInteractionEnabled = true
            tfDimValue.isUserInteractionEnabled = true
            tfCctValue.isUserInteractionEnabled = true
            btnSelectLed.setImage(checkImage, for: .normal)
            if data.id == nil {
                data.id = newSelectedDevice
                data.type = ExecuteType.relay.rawValue
                data.during = Int(tfDelay.text ?? "") ?? 1
                data.delay = Int(tfDuring.text ?? "") ?? 0
                data.dim = Int(tfDimValue.text ?? "") ?? 1
                data.cct = Int(tfCctValue.text ?? "") ?? 0
            }
            delegate?.isLedSelected(device: data, isSelect: true)
        } else {
            tfDelay.isUserInteractionEnabled = false
            tfDuring.isUserInteractionEnabled = false
            tfDimValue.isUserInteractionEnabled = false
            tfCctValue.isUserInteractionEnabled = false
            btnSelectLed.setImage(uncheckImage, for: .normal)
            delegate?.isLedSelected(device: data, isSelect: false)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isNotEmpty ?? false {
            if textField == tfDelay {
                data.delay = Int(tfDelay.text ?? "") ?? 0
                delegate?.ledValueDidChange(device: data)
            } else if textField == tfDuring {
                data.during = Int(tfDuring.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: data)
            } else if textField == tfDimValue {
                data.dim = Int(tfDimValue.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: data)
            } else if textField == tfCctValue {
                data.cct = Int(tfCctValue.text ?? "") ?? 1
                delegate?.ledValueDidChange(device: data)
            }
        }
    }
}

extension SelectingLedCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
