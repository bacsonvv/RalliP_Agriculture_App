//
//  AddEditScheduleViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 12/11/2021.
//

import UIKit
import IBAnimatable

class AddEditRuleViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadowView: AnimatableView!
    
    var screenType = ScreenType.add.rawValue
    var deviceType = DeviceType.relay.rawValue
    var ruleType = RuleType.schedule.rawValue
    var outputSelectionType = ExecuteType.relay.rawValue
    
    private var addEditNameCellHeight: CGFloat = 118
    private var addEditStatusCellHeight: CGFloat = 118
    private var addEditLightPropertyCellHeight: CGFloat = 207.5
    private var addEditTimeCellHeight: CGFloat = 118
    private var addEditRepeatCellHeight: CGFloat = 359
    private var addEditDeviceCellHeightWithDevice: CGFloat = 326
    private var addEditUpdateTimeCellHeight: CGFloat = 118
    private var addEditLogicCellHeight: CGFloat = 118
    private var addEditSelectInputDeviceCellHeight: CGFloat = 70
    private var addEditSelectOutputDeviceCellHeight: CGFloat = 118

    var data = RuleModel()
    var dataPutWithOutputDevice = RuleModel()
    var dataPutWithOutputScript = RuleModel()
    var relay = RelayModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defer {
            if isViewLoaded {
                view.bringSubviewToFront(shadowView)
            }
        }
        
        if screenType == ScreenType.add.rawValue && ruleType == RuleType.schedule.rawValue {
            let execute = Execute()
            data.execute.append(execute)
        }
        
        setupTableView()
        setupScreenTitle()
        
        if ruleType == RuleType.rule.rawValue {
            if screenType == ScreenType.edit.rawValue {
                if data.execute.isNotEmpty {
                    let execute = data.execute[0]
                    if execute.type == ExecuteType.relay.rawValue {
                        outputSelectionType = ExecuteType.relay.rawValue
                        dataPutWithOutputScript.execute.removeAll()
                    } else {
                        outputSelectionType = ExecuteType.script.rawValue
                        dataPutWithOutputDevice.execute.removeAll()
                    }
                } else {
                    outputSelectionType = ExecuteType.relay.rawValue
                }
            } else {
                outputSelectionType = ExecuteType.relay.rawValue
            }
        }
    }
    
    func setupScreenTitle() {
        var suffix = ""
        switch ruleType {
        case RuleType.script.rawValue:
            suffix = "kịch bản"
        case RuleType.schedule.rawValue:
            suffix = "lịch"
        case RuleType.rule.rawValue:
            suffix = "luật"
        default:
            break
        }
        lblBaseScreenTitle.text = screenType + " " + suffix
    }
    
    func setupTableView() {
        AddEditNameCell.registerCellByNib(tableView)
        AddEditStatusCell.registerCellByNib(tableView)
        AddEditLightPropertyCell.registerCellByNib(tableView)
        AddEditTimeCell.registerCellByNib(tableView)
        AddEditRepeatCell.registerCellByNib(tableView)
        AddEditDeviceCell.registerCellByNib(tableView)
        AddEditUpdateTimeCell.registerCellByNib(tableView)
        AddEditLogicCell.registerCellByNib(tableView)
        AddEditSelectInputDeviceCell.registerCellByNib(tableView)
        AddEditSelectOutputDeviceCell.registerCellByNib(tableView)
        tableView.delegate = self
        tableView.dataSource = self
    }
}

// MARK: IBActions
extension AddEditRuleViewController {
    @IBAction func clicktoFinish(_ sender: Any) {
        switch ruleType {
        case RuleType.schedule.rawValue:
            if screenType == ScreenType.edit.rawValue {
                guard let id = data.id else { return  }
                RuleRouter(endpoint: .updateSchedule(id: id, data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            } else {
                // Prepare data for adding schedule
                data.active = true
                if data.execute.isNotEmpty {
                    data.execute[0].id = relay
                    data.execute[0].type = "Relay"
                    data.type = ruleType
                }
                
                RuleRouter(endpoint: .addSchedule(data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            }
        case RuleType.script.rawValue:
            if screenType == ScreenType.edit.rawValue {
                guard let id = data.id else { return  }
                RuleRouter(endpoint: .updateScript(id: id, data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            } else {
                // Prepare data for adding script
                data.active = false
                data.type = ruleType
                
                RuleRouter(endpoint: .addScript(data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            }
        default:
            if screenType == ScreenType.edit.rawValue {
                if outputSelectionType == ExecuteType.relay.rawValue {
                    if dataPutWithOutputDevice.execute.isNotEmpty {
                        data = dataPutWithOutputDevice
                    }
                } else {
                    if dataPutWithOutputScript.execute.isNotEmpty {
                        data = dataPutWithOutputScript
                    }
                }
                
                guard let id = data.id else { return  }
                RuleRouter(endpoint: .updateRule(id: id, data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            } else {
                // Prepare data for adding rule
                data.active = true
                data.type = ruleType
                if outputSelectionType == ExecuteType.relay.rawValue {
                    data.execute = dataPutWithOutputDevice.execute
                } else {
                    data.execute = dataPutWithOutputScript.execute
                }
                
                RuleRouter(endpoint: .addRule(data: data)).mapTo(Bool.self, success: { [weak self] (isSuccess) in
                    guard let weakSelf = self else { return }
                    if isSuccess {
                        DispatchQueue.main.async {
                            guard let navigationController = weakSelf.navigationController else { return }
                            navigationController.popViewController(animated: true)
                        }
                    }
                })
            }
        }
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension AddEditRuleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch ruleType {
        case RuleType.schedule.rawValue:
            return 4
        case RuleType.script.rawValue:
            return 2
        case RuleType.rule.rawValue:
            return 8
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = AddEditNameCell.loadCell(tableView) as? AddEditNameCell else { return UITableViewCell() }
            cell.parentViewController = self
            if let scheduleName = data.name {
                cell.setupData(scheduleName: scheduleName, ruleType: ruleType)
            } else {
                cell.setupData(scheduleName: "", ruleType: ruleType)
            }
            return cell
        case 1:
            switch ruleType {
            case RuleType.schedule.rawValue:
                if deviceType == DeviceType.relay.rawValue {
                    guard let cell = AddEditStatusCell.loadCell(tableView) as? AddEditStatusCell else { return UITableViewCell() }
                    cell.parentViewController = self
                    if screenType == ScreenType.edit.rawValue {
                        if data.execute.count > 0 {
                            if let pull = data.execute[0].pull {
                                cell.setupData(state: pull, screenType: screenType)
                            }
                        }
                    } else {
                        cell.setupData(state: 0, screenType: screenType)
                    }
                    return cell
                } else {
                    guard let cell = AddEditLightPropertyCell.loadCell(tableView) as? AddEditLightPropertyCell else { return UITableViewCell() }
                    cell.setupData(schedule: data)
                    cell.parentViewController = self
                    return cell
                }
            case RuleType.script.rawValue:
                guard let cell = AddEditDeviceCell.loadCell(tableView) as? AddEditDeviceCell else { return UITableViewCell() }
                cell.data = data
                cell.screenType = screenType
                cell.parentViewController = self
                return cell
            case RuleType.rule.rawValue:
                guard let cell = AddEditTimeCell.loadCell(tableView) as? AddEditTimeCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.timeType = TimeType.start.rawValue
                cell.setupData(rule: data, screenType: screenType)
                return cell
            default:
                return UITableViewCell()
            }
        case 2:
            switch ruleType {
            case RuleType.schedule.rawValue:
                guard let cell = AddEditTimeCell.loadCell(tableView) as? AddEditTimeCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.timeType = TimeType.start.rawValue
                cell.setupData(rule: data, screenType: screenType)
                return cell
            case RuleType.script.rawValue:
                return UITableViewCell()
            case RuleType.rule.rawValue:
                guard let cell = AddEditTimeCell.loadCell(tableView) as? AddEditTimeCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.timeType = TimeType.stop.rawValue
                cell.setupData(rule: data, screenType: screenType)
                return cell
            default:
                return UITableViewCell()
            }
        case 3:
            switch ruleType {
            case RuleType.schedule.rawValue:
                guard let cell = AddEditRepeatCell.loadCell(tableView) as? AddEditRepeatCell else { return UITableViewCell() }
                cell.parentViewController = self
                if screenType == ScreenType.edit.rawValue {
                    cell.setupData(schedule: data)
                }
                return cell
            case RuleType.script.rawValue:
                return UITableViewCell()
            case RuleType.rule.rawValue:
                guard let cell = AddEditUpdateTimeCell.loadCell(tableView) as? AddEditUpdateTimeCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.setupData(rule: data, screenType: screenType)
                return cell
            default:
                return UITableViewCell()
            }
        case 4:
            if ruleType == RuleType.rule.rawValue {
                guard let cell = AddEditLogicCell.loadCell(tableView) as? AddEditLogicCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.setupData(rule: data)
                return cell
            }
            return UITableViewCell()
        case 5:
            if ruleType == RuleType.rule.rawValue {
                guard let cell = AddEditSelectInputDeviceCell.loadCell(tableView) as? AddEditSelectInputDeviceCell else { return UITableViewCell() }
                cell.parentViewController = self
                return cell
            }
            return UITableViewCell()
        case 6:
            if ruleType == RuleType.rule.rawValue {
                guard let cell = AddEditSelectOutputDeviceCell.loadCell(tableView) as? AddEditSelectOutputDeviceCell else { return UITableViewCell() }
                cell.setupData(data: data, screenType: screenType)
                cell.delegate = self
                cell.parentViewController = self
                return cell
            }
            return UITableViewCell()
        case 7:
            if ruleType == RuleType.rule.rawValue {
                guard let cell = AddEditRepeatCell.loadCell(tableView) as? AddEditRepeatCell else { return UITableViewCell() }
                cell.parentViewController = self
                cell.setupData(schedule: data)
                return cell
            }
            return UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return addEditNameCellHeight
        case 1:
            switch ruleType {
            case RuleType.schedule.rawValue:
                if deviceType == DeviceType.relay.rawValue {
                    return addEditStatusCellHeight
                }
                return addEditLightPropertyCellHeight
            case RuleType.script.rawValue:
                return addEditDeviceCellHeightWithDevice
            case RuleType.rule.rawValue:
                return addEditTimeCellHeight
            default:
                return 0
            }
        case 2:
            switch ruleType {
            case RuleType.schedule.rawValue:
                return addEditTimeCellHeight
            case RuleType.script.rawValue:
                return addEditRepeatCellHeight
            case RuleType.rule.rawValue:
                return addEditTimeCellHeight
            default:
                return 0
            }
        case 3:
            switch ruleType {
            case RuleType.schedule.rawValue:
                return addEditRepeatCellHeight
            case RuleType.script.rawValue:
                return addEditUpdateTimeCellHeight
            case RuleType.rule.rawValue:
                return addEditUpdateTimeCellHeight
            default:
                return 0
            }
        case 4:
            if ruleType == RuleType.rule.rawValue {
                return addEditLogicCellHeight
            }
            return 0
        case 5:
            if ruleType == RuleType.rule.rawValue {
                return addEditSelectInputDeviceCellHeight
            }
            return 0
        case 6:
            if ruleType == RuleType.rule.rawValue {
                return addEditSelectOutputDeviceCellHeight
            }
            return 0
        case 7:
            if ruleType == RuleType.rule.rawValue {
                return addEditRepeatCellHeight
            }
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 5:
            if ruleType == RuleType.rule.rawValue {
                guard let selectInputDeviceViewController = R.storyboard.selectInputDevice.selectInputDeviceViewController() else { return }
                selectInputDeviceViewController.data = data
                selectInputDeviceViewController.delegate = self
                guard let navigationController = self.navigationController else { return }
                navigationController.pushViewController(selectInputDeviceViewController, animated: true)
            }
        case 6:
            if ruleType == RuleType.rule.rawValue {
                if outputSelectionType == ExecuteType.relay.rawValue {
                    guard let selectOutputDeviceViewController = R.storyboard.selectOutputDevice.selectOutputDeviceViewController() else { return }
                    selectOutputDeviceViewController.rule = dataPutWithOutputDevice
                    selectOutputDeviceViewController.delegate = self
                    guard let navigationController = self.navigationController else { return }
                    navigationController.pushViewController(selectOutputDeviceViewController, animated: true)
                } else {
                    guard let selectScriptOutputViewController = R.storyboard.selectOutputScript.selectOutputScriptViewController() else { return }
                    selectScriptOutputViewController.rule = dataPutWithOutputScript
                    selectScriptOutputViewController.delegate = self
                    guard let navigationController = self.navigationController else { return }
                    navigationController.pushViewController(selectScriptOutputViewController, animated: true)
                }
            }
        default:
            return
        }
    }
}

// MARK: Override functions
extension AddEditRuleViewController {
    override func goBackToPreviousViewController() {
        guard let navigationController = self.navigationController else { return }
        navigationController.popViewController(animated: true)
    }
}

// MARK: CELL DELEGATE
extension AddEditRuleViewController: SelectInputDeviceViewControllerDelegate, SelectOutputDeviceViewControllerDelegate, SelectOutputScriptViewControllerDelegate {
    func finishSelectOutputDevice(returnData: RuleModel) {
        dataPutWithOutputDevice = returnData
    }
    
    func finishSelectOutputScript(returnData: RuleModel) {
        dataPutWithOutputScript = returnData
    }
    
    func finishSelectInput(returnData: RuleModel) {
        if outputSelectionType == ExecuteType.relay.rawValue {
            dataPutWithOutputDevice = returnData
        } else {
            dataPutWithOutputScript = returnData
        }
    }
}

extension AddEditRuleViewController: AddEditSelectOutputDeviceCellDelegate {
    func outputSelectionTypeDidChange(type: String) {
        outputSelectionType = type
    }
}
