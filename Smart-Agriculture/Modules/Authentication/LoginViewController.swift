//
//  LoginViewController.swift
//  Smart-Agriculture
//
//  Created by admin on 26/10/2021.
//

import UIKit

class LoginViewController: BaseViewController {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private var username: String = UserDefaults.standard.string(forKey: "username") ?? ""
    private var password: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupButton()
        setupTextField()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -100 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func setupButton() {
        disabledButton()
    }
    
    func disabledButton() {
        btnLogin.customize(backgroundColor: UIColor.colorFromHexString(hex: "#B8E5E6"), tintColor: .white, cornerRadius: btnLogin.layer.frame.height / 2)
        btnLogin.isUserInteractionEnabled = false
    }
    
    func enableButton() {
        btnLogin.customize(backgroundColor: UIColor.colorFromHexString(hex: "#2C698D"), tintColor: .white, cornerRadius: btnLogin.layer.frame.height / 2)
        btnLogin.isUserInteractionEnabled = true
    }
    
    func setupTextField() {
        emailTextField.text = username
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

// MARK: Handle IBActions
extension LoginViewController {
    @IBAction func clickToLogin(_ sender: Any) {
        AuthenticationRouter(endpoint: .login(username: username, password: password)).mapTo(UserModel.self, success: { [weak self] (user) in
            guard let weakSelf = self else { return }
            UserModel.shared.save(user)
            DispatchQueue.main.async {
                guard let greenHouseId = user.userInfo?.greenHouseId else { return }
                GreenHouseRouter(endpoint: .getAGreenHouse(greenHouseId: greenHouseId)).mapTo(GreenHouseModel.self, success: {
                    [weak weakSelf] (greenHouse) in
                    guard let weakSelf1 = weakSelf else { return }
                    GreenHouseModel.shared.saveGreenHouse(greenHouse)
                    DispatchQueue.main.async {
                        guard let mainViewController = R.storyboard.sideMenu.mainViewController() else { return }
                        weakSelf1.navigationController?.pushViewController(mainViewController, animated: false)
                    }
                })
            }
        }, unauthorized: { [weak self] (unauthorized) in
            guard let weakSelf = self else { return }
            DispatchQueue.main.async {
                weakSelf.hideLoading()
                if let error = unauthorized as? ErrorModel {
                    if let _ = error.code, let errorMessage = error.message {
                        let alert = UIAlertController(title: ConstantText.alertTitle, message: errorMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
                        weakSelf.present(alert, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    @IBAction func clickToForgotPassword(_ sender: Any) {
        let alert = UIAlertController(title: ConstantText.alertTitle, message: ConstantText.forgotPasswordMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ConstantText.okAlertButtonTitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (emailTextField.text?.isEmpty ?? true) || (passwordTextField.text?.isEmpty ?? true) {
            disabledButton()
        } else {
            username = emailTextField.text ?? ""
            password = passwordTextField.text ?? ""
            enableButton()
        }
    }
}
