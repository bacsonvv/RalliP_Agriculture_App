//
//  UserModel.swift
//  Smart-Agriculture
//
//  Created by Bac Son on 25/11/2021.
//

import Foundation
import SwiftyJSON

class UserModel: BaseModel {
    var userInfo: UserInfo?
    var token: Token?
    
    static var shared = UserModel()
    var currentUser: UserModel?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        userInfo = UserInfo(fromJSON: json["user"])
        token = Token(fromJSON: json["tokens"])
    }
}

class UserInfo: BaseModel {
    var avatar: String?
    var active: Bool = false
    var role: Int?
    var username: String?
    var name: String?
    var createdAt: String?
    var id: String?
    var greenHouseId: String?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        avatar = json["avatar"].string
        active = json["active"].bool ?? false
        role = json["role"].int
        username = json["username"].string
        name = json["name"].string
        createdAt = json["createdAt"].string
        id = json["id"].string
        greenHouseId = json["greenhouseId"].string
    }
}

class Token: BaseModel {
    var accessToken: AccessToken?
    var refreshToken: RefreshToken?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        accessToken = AccessToken(fromJSON: json["access"])
        refreshToken = RefreshToken(fromJSON: json["refresh"])
    }
}

class AccessToken: BaseModel {
    var token: String?
    var expires: String?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        token = json["token"].string
        expires = json["expires"].string
    }
}

class RefreshToken: BaseModel {
    var token: String?
    var expires: String?
    
    override func mapping(fromJSON json: JSON) {
        super.mapping(fromJSON: json)
        token = json["token"].string
        expires = json["expires"].string
    }
}

// MARK: Public methods
extension UserModel {
    func save(_ user: UserModel) {
        currentUser = user
        UserDefaults.standard.set(user.userInfo?.username, forKey: "username")
    }
    
    func deleteAllData() {
        currentUser = nil
    }
}
